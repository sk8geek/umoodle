# An API for Moodle implemented in Micronaut

The source files/documents used in creating this application are 
from the 3.8 version of Moodle. However, my installed version 
(on an old Raspberry Pi) that I'm running the application against 
is version 3.6.

## To Do

One of the more time consuming tasks is to add annotations around 
the relationships between `Entity` objects. I created these from 
the distributed `install.xml` files and they are packaged to match. 
The largest group is the `lib` package. This is split into 
sub-packages to group the files logically. 

I have started working through the `lib` package and will use this 
list to indicate progress.

- [x] admin
- [x] auth
- [x] blocks
- [x] enrol
- [x] grade
- [ ] lib
- [x] message
- [x] mnet
- [ ] mod
- [x] portfolio
- [x] question
- [x] repository
- [x] search
