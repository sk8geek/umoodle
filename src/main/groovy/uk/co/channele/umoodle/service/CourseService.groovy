/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.service

import uk.co.channele.umoodle.MsgService
import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.exception.NotFoundException
import uk.co.channele.umoodle.repository.CourseRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CourseService {

    @Inject
    CourseRepo courseRepo

    @Inject
    MsgService msg

    Course get(Long id) {
        def optionalCourse = courseRepo.findById(id)
        if (optionalCourse.present) {
            return optionalCourse.get()
        }
        throw new NotFoundException(msg.get("exception.notFound"))
    }

    List<Course> getCourses() {
        return courseRepo.findAll()
    }

}
