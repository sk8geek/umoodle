/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.service

import uk.co.channele.umoodle.domain.lib.repository.Repository
import uk.co.channele.umoodle.repository.FileRepo
import uk.co.channele.umoodle.repository.RepositoryRepo
import uk.co.channele.umoodle.domain.lib.file.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryService {

    @Inject
    RepositoryRepo repositoryRepo

    @Inject
    FileRepo fileRepo

    List<Repository> getRepositories() {
        return repositoryRepo.findAll()
    }

    List<File> getFiles() {
        return fileRepo.findAll()
    }
}
