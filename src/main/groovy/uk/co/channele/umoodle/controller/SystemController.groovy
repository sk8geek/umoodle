/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.controller

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.scheduling.TaskExecutors
import io.micronaut.scheduling.annotation.ExecuteOn
import uk.co.channele.umoodle.domain.lib.system.Capabilities
import uk.co.channele.umoodle.service.SystemService

import javax.inject.Inject

@Controller("/system")
class SystemController {

    @Inject
    SystemService service

    @Get("/capabilities")
    @ExecuteOn(TaskExecutors.IO)
    HttpResponse<List<Capabilities>> index() {
        return HttpResponse.ok(service.capabilities)
    }

}
