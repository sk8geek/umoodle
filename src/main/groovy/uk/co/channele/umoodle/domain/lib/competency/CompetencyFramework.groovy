/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.competency

import uk.co.channele.umoodle.domain.lib.context.Context
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "competency_framework")
class CompetencyFramework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 100)
    String shortname = ""

    @Column(length = 100, unique = true)
    String idnumber = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer descriptionformat = 0

    @Column(name = "scaleid",  columnDefinition = "bigint(11)")
    Integer scaleId

    @Column(columnDefinition = "longtext", nullable = false)
    String scaleconfiguration = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer visible = 1

    @Column(length = 255, nullable = false)
    String taxonomies = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column
    Long usermodified

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @OneToMany(mappedBy = "competencyFramework")
    Set<Competency> competencySet

    @ManyToOne
    @JoinColumn(name = "contextid", nullable = false)
    Context context

}
