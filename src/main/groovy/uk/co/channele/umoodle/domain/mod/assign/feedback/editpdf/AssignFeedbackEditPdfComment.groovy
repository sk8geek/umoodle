/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assign.feedback.editpdf

import uk.co.channele.umoodle.domain.mod.assign.AssignGrade

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assignfeedback_editpdf_cmnt")
class AssignFeedbackEditPdfComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "x", columnDefinition = "bigint(10)")
    Long x = 0

    @Column(name = "y", columnDefinition = "bigint(10)")
    Long y = 0

    @Column(name = "width", columnDefinition = "bigint(10)")
    Long width = 120

    @Column(name = "rawtext", columnDefinition = "longtext")
    String rawtext

    @Column(name = "pageno", columnDefinition = "bigint(10)", nullable = false)
    Long pageno = 0

    @Column(name = "colour", length = 10)
    String colour

    @Column(name = "draft", columnDefinition = "tinyint(2)", nullable = false)
    Integer draft = 1


    @OneToOne()
    @JoinColumn(name = "gradeid", columnDefinition = "bigint(10)", nullable = false)
    AssignGrade grade

}
