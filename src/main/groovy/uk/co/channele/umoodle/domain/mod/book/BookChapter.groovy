/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.book

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="book_chapters")
class BookChapter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "pagenum", columnDefinition = "bigint(10)", nullable = false)
    Long pageNum = 0

    @Column(name = "subchapter", columnDefinition = "bigint(10)", nullable = false)
    Long subChapter = 0

    @Column(name = "title", length = 255, nullable = false)
    String title

    @Column(name = "content", columnDefinition = "longtext", nullable = false)
    String content

    @Column(name = "contentformat", columnDefinition = "smallint(4)", nullable = false)
    Integer contentFormat = 0

    @Column(name = "hidden", columnDefinition = "tinyint(2)", nullable = false)
    Integer hidden = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "importsrc", length = 255, nullable = false)
    String importSrc


    @ManyToOne()
    @JoinColumn(name = "bookid", columnDefinition = "bigint(10)", nullable = false)
    Book book

}
