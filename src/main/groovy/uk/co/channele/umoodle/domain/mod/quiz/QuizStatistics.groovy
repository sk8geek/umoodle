/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.quiz

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="quiz_statistics")
class QuizStatistics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "hashcode", length = 40, nullable = false)
    String hashcode

    @Column(name = "whichattempts", columnDefinition = "smallint(4)", nullable = false)
    Integer whichAttempts

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified

    @Column(name = "firstattemptscount", columnDefinition = "bigint(10)", nullable = false)
    Long firstAttemptsCount

    @Column(name = "highestattemptscount", columnDefinition = "bigint(10)", nullable = false)
    Long highestAttemptsCount

    @Column(name = "lastattemptscount", columnDefinition = "bigint(10)", nullable = false)
    Long lastAttemptsCount

    @Column(name = "allattemptscount", columnDefinition = "bigint(10)", nullable = false)
    Long allAttemptsCount

    @Column(name = "firstattemptsavg", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal firstAttemptsAvg

    @Column(name = "highestattemptsavg", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal highestAttemptsAvg

    @Column(name = "lastattemptsavg", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal lastAttemptsAvg

    @Column(name = "allattemptsavg", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal allAttemptsAvg

    @Column(name = "median", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal median

    @Column(name = "standarddeviation", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal standardDeviation

    @Column(name = "skewness", columnDefinition = "decimal(15,10)", nullable = false)
    BigDecimal skewness

    @Column(name = "kurtosis", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal kurtosis

    @Column(name = "cic", columnDefinition = "decimal(15,10)", nullable = false)
    BigDecimal cic

    @Column(name = "errorratio", columnDefinition = "decimal(15,10)", nullable = false)
    BigDecimal errorRatio

    @Column(name = "standarderror", columnDefinition = "decimal(15,10)", nullable = false)
    BigDecimal standardError

}