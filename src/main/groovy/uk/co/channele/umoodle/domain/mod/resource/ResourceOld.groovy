/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.resource

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="resource_old")
class ResourceOld {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "type", length = 30, nullable = false)
    String type

    @Column(name = "reference", length = 255, nullable = false)
    String reference

    @Column(name = "intro", columnDefinition = "longtext")
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introFormat = 0

    @Column(name = "alltext", columnDefinition = "longtext", nullable = false)
    String allText

    @Column(name = "popup", columnDefinition = "longtext", nullable = false)
    String popup

    @Column(name = "options", length = 255, nullable = false)
    String options

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "oldid", columnDefinition = "bigint(10)", nullable = false)
    Long oldid

    @Column(name = "cmid", columnDefinition = "bigint(10)")
    Long cmid

    @Column(name = "newmodule", length = 50)
    String newmodule

    @Column(name = "newid", columnDefinition = "bigint(10)")
    Long newid

    @Column(name = "migrated", columnDefinition = "bigint(10)", nullable = false)
    Long migrated = 0


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

//        <INDEX NAME="oldid" UNIQUE="true" FIELDS="oldid"/>
//        <INDEX NAME="cmid" UNIQUE="false" FIELDS="cmid"/>

}
