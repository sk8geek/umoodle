/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.essay

import uk.co.channele.umoodle.domain.lib.question.Question

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="qtype_essay_options")
class QTypeEssayOptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "responseformat", length = 16, nullable = false)
    String responseformat

    @Column(name = "responserequired", columnDefinition = "tinyint(2)", nullable = false)
    Integer responserequired = 1

    @Column(name = "responsefieldlines", columnDefinition = "smallint(4)", nullable = false)
    Integer responsefieldlines = 15

    @Column(name = "attachments", columnDefinition = "smallint(4)", nullable = false)
    Integer attachments = 0

    @Column(name = "attachmentsrequired", columnDefinition = "smallint(4)", nullable = false)
    Integer attachmentsrequired = 0

    @Column(name = "graderinfo", columnDefinition = "longtext")
    String graderinfo

    @Column(name = "graderinfoformat", columnDefinition = "smallint(4)", nullable = false)
    Integer graderinfoformat = 0

    @Column(name = "responsetemplate", columnDefinition = "longtext")
    String responsetemplate

    @Column(name = "responsetemplateformat", columnDefinition = "smallint(4)", nullable = false)
    Integer responsetemplateformat = 0

    @Column(name = "filetypeslist", columnDefinition = "longtext")
    String filetypeslist


    @ManyToOne()
    @JoinColumn(name = "questionid", columnDefinition = "bigint(10)", nullable = false)
    Question question

}
