/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.feedback

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="feedback_value", uniqueConstraints = @UniqueConstraint(columnNames = ["completed", "item", "course_id"]))
class FeedbackValue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "completed", columnDefinition = "bigint(10)", nullable = false)
    Long completed = 0

    @Column(name = "tmp_completed", columnDefinition = "bigint(10)", nullable = false)
    Long tmp_completed = 0

    @Column(name = "value", columnDefinition = "longtext", nullable = false)
    String value


    @OneToOne()
    @JoinColumn(name = "item", columnDefinition = "bigint(10)", nullable = false)
    FeedbackItem item

    @OneToOne()
    @JoinColumn(name = "course_id", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
