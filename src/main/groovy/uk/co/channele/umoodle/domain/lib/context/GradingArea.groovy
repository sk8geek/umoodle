/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.context

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "grading_areas", uniqueConstraints = @UniqueConstraint(columnNames = ["contextid", "component", "areaname"]))
class GradingArea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "component", length = 100, nullable = false)
    String component = ""

    @Column(name = "areaname", length = 100, nullable = false)
    String areaName = ""

    @Column(name = "activemethod", length = 100)
    String activeMethod = ""


    @OneToMany(mappedBy = "gradingArea")
    Set<GradingDefinition> definitionSet

    @ManyToOne()
    @JoinColumn(name = "contextid", nullable = false)
    Context context


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="uq_gradable_area" TYPE="unique" FIELDS=/>
// <KEY NAME="fk_context" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>
}