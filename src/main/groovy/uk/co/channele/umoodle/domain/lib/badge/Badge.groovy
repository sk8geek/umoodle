/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.badge

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "badge")
class Badge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(name = "issuername", length = 255, nullable = false)
    String issuerName = ""

    @Column(name = "issuerurl", length = 255, nullable = false)
    String issuerUrl = ""

    @Column(name = "issuercontact", length = 255)
    String issuerContact = ""

    @Column(name = "expiredate")
    Long expireDate

    @Column(name = "expireperiod")
    Long expirePeriod

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer type = 1

    @Column(columnDefinition = "longtext", nullable = false)
    String message = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String messagesubject = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer attachment = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer notification = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer status = 0

    @Column()
    Long nextcron

    @Column(length = 255)
    String version = ""

    @Column(length = 255)
    String language = ""

    @Column(length = 255)
    String imageauthorname = ""

    @Column(length = 255)
    String imageauthoremail = ""

    @Column(length = 255)
    String imageauthorurl = ""

    @Column(columnDefinition = "longtext")
    String imagecaption = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @ManyToOne
    @JoinColumn(name = "courseid")
    Course course

    @OneToOne
    @JoinColumn(name = "usercreated", nullable = false)
    User userCreated

    @OneToMany(mappedBy = "badge")
    Set<BadgeEndorsement> endorsementSet

    @OneToMany(mappedBy = "badge")
    Set<BadgeRelated> relatedSet

    @OneToMany(mappedBy = "badge")
    Set<BadgeCriteria> criteriaSet

    @OneToMany(mappedBy = "badge")
    Set<BadgeIssued> issuedSet

    @OneToMany(mappedBy = "badge")
    Set<BadgeManualAward> manualAwardSet

}
