/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.enrol.lti

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="enrol_lti_lti2_consumer")
class EnrolLtiLti2Consumer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 50, nullable = false)
    String name

    @Column(name = "consumerkey256", length = 255, nullable = false, unique = true)
    String consumerkey256

    @Column(name = "consumerkey", columnDefinition = "longtext")
    String consumerkey

    @Column(name = "secret", length = 1024, nullable = false)
    String secret

    @Column(name = "ltiversion", length = 10)
    String ltiversion

    @Column(name = "consumername", length = 255)
    String consumername

    @Column(name = "consumerversion", length = 255)
    String consumerversion

    @Column(name = "consumerguid", length = 1024)
    String consumerguid

    @Column(name = "profile", columnDefinition = "longtext")
    String profile

    @Column(name = "toolproxy", columnDefinition = "longtext")
    String toolproxy

    @Column(name = "settings", columnDefinition = "longtext")
    String settings

    @Column(name = "protected", columnDefinition = "bit(1)", nullable = false)
    Boolean consumerProtected

    @Column(name = "enabled", columnDefinition = "bit(1)", nullable = false)
    Boolean enabled

    @Column(name = "enablefrom", columnDefinition = "bigint(10)")
    Long enablefrom

    @Column(name = "enableuntil", columnDefinition = "bigint(10)")
    Long enableuntil

    @Column(name = "lastaccess", columnDefinition = "bigint(10)")
    Long lastaccess

    @Column(name = "created", columnDefinition = "bigint(10)", nullable = false)
    Long created

    @Column(name = "updated", columnDefinition = "bigint(10)", nullable = false)
    Long updated

}
