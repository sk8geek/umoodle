/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.context

import uk.co.channele.umoodle.domain.grade.grading.form.guide.GradingFormGuideComments
import uk.co.channele.umoodle.domain.grade.grading.form.guide.GradingFormGuideCriteria
import uk.co.channele.umoodle.domain.grade.grading.form.rubric.GradingFormRubricCriteria
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "grading_definitions", uniqueConstraints = @UniqueConstraint(columnNames = ["areaid", "method"]))
class GradingDefinition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 100, nullable = false)
    String method = ""

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(name = "descriptionformat", columnDefinition = "tinyint")
    Integer descriptionFormat

    @Column(nullable = false)
    Long status = 0

    @Column(name = "copiedfromid")
    Long copiedFromId

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(name = "timecopied")
    Long timeCopied = 0

    @Column(name = "options", columnDefinition = "longtext")
    String options = ""


    @OneToMany(mappedBy = "definition")
    Set<GradingFormGuideComments> formGuideCommentsSet

    @OneToMany(mappedBy = "definition")
    Set<GradingFormGuideCriteria> formGuideCriteriaSet

    @OneToMany(mappedBy = "definition")
    Set<GradingFormRubricCriteria> formRubricCriteriaSet

    @ManyToOne()
    @JoinColumn(name = "areaid", nullable = false)
    GradingArea gradingArea

    @OneToOne()
    @JoinColumn(name = "usercreated", nullable = false)
    User createdBy

    @OneToOne()
    @JoinColumn(name = "usermodified", nullable = false)
    User modifiedBy



// <KEY NAME="fk_areaid" TYPE="foreign" FIELDS="areaid" REFTABLE="grading_areas" REFFIELDS="id"/>
// <KEY NAME="fk_usermodified" TYPE="foreign" FIELDS="usermodified" REFTABLE="user" REFFIELDS="id"/>
// <KEY NAME="uq_area_method" TYPE="unique" FIELDS=/>
// <KEY NAME="fk_usercreated" TYPE="foreign" FIELDS="usercreated" REFTABLE="user" REFFIELDS="id"/>
}
