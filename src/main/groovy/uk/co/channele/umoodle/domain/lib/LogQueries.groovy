/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "log_queries")
class LogQueries {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "mediumint", nullable = false)
    Integer qtype

    @Column(columnDefinition = "longtext", nullable = false)
    String sqltext = ""

    @Column(columnDefinition = "longtext")
    String sqlparams = ""

    @Column(columnDefinition = "mediumint", nullable = false)
    Integer error = 0

    @Column(columnDefinition = "longtext")
    String info = ""

    @Column(columnDefinition = "longtext")
    String backtrace = ""

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal exectime

    @Column(nullable = false)
    Long timelogged


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
}
