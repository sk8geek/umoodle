/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.quiz

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="quiz")
class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "timeopen", columnDefinition = "bigint(10)", nullable = false)
    Long timeopen = 0

    @Column(name = "timeclose", columnDefinition = "bigint(10)", nullable = false)
    Long timeclose = 0

    @Column(name = "timelimit", columnDefinition = "bigint(10)", nullable = false)
    Long timelimit = 0

    @Column(name = "overduehandling", length = 16, nullable = false)
    String overduehandling

    @Column(name = "graceperiod", columnDefinition = "bigint(10)", nullable = false)
    Long graceperiod = 0

    @Column(name = "preferredbehaviour", length = 32, nullable = false)
    String preferredbehaviour

    @Column(name = "canredoquestions", columnDefinition = "smallint(4)", nullable = false)
    Integer canredoquestions = 0

    @Column(name = "attempts", columnDefinition = "mediumint(6)", nullable = false)
    Integer attempts = 0

    @Column(name = "attemptonlast", columnDefinition = "smallint(4)", nullable = false)
    Integer attemptonlast = 0

    @Column(name = "grademethod", columnDefinition = "smallint(4)", nullable = false)
    Integer grademethod = 1

    @Column(name = "decimalpoints", columnDefinition = "smallint(4)", nullable = false)
    Integer decimalpoints = 2

    @Column(name = "questiondecimalpoints", columnDefinition = "smallint(4)", nullable = false)
    Integer questiondecimalpoints = --1

    @Column(name = "reviewattempt", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewattempt = 0

    @Column(name = "reviewcorrectness", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewcorrectness = 0

    @Column(name = "reviewmarks", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewmarks = 0

    @Column(name = "reviewspecificfeedback", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewspecificfeedback = 0

    @Column(name = "reviewgeneralfeedback", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewgeneralfeedback = 0

    @Column(name = "reviewrightanswer", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewrightanswer = 0

    @Column(name = "reviewoverallfeedback", columnDefinition = "mediumint(6)", nullable = false)
    Integer reviewoverallfeedback = 0

    @Column(name = "questionsperpage", columnDefinition = "bigint(10)", nullable = false)
    Long questionsperpage = 0

    @Column(name = "navmethod", length = 16, nullable = false)
    String navmethod

    @Column(name = "shuffleanswers", columnDefinition = "smallint(4)", nullable = false)
    Integer shuffleanswers = 0

    @Column(name = "sumgrades", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal sumgrades = 0

    @Column(name = "grade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grade = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "password", length = 255, nullable = false)
    String password

    @Column(name = "subnet", length = 255, nullable = false)
    String subnet

    @Column(name = "browsersecurity", length = 32, nullable = false)
    String browsersecurity

    @Column(name = "delay1", columnDefinition = "bigint(10)", nullable = false)
    Long delay1 = 0

    @Column(name = "delay2", columnDefinition = "bigint(10)", nullable = false)
    Long delay2 = 0

    @Column(name = "showuserpicture", columnDefinition = "smallint(4)", nullable = false)
    Integer showuserpicture = 0

    @Column(name = "showblocks", columnDefinition = "smallint(4)", nullable = false)
    Integer showblocks = 0

    @Column(name = "completionattemptsexhausted", columnDefinition = "bit(1)")
    Boolean completionattemptsexhausted = false

    @Column(name = "completionpass", columnDefinition = "bit(1)")
    Boolean completionpass = false

    @Column(name = "allowofflineattempts", columnDefinition = "bit(1)")
    Boolean allowofflineattempts = false


    @OneToMany(mappedBy = "quiz")
    Set<QuizAttempt> attemptSet

    @OneToMany(mappedBy = "quiz")
    Set<QuizFeedback> feedbackSet

    @OneToMany(mappedBy = "quiz")
    Set<QuizGrade> gradeSet

    @OneToMany(mappedBy = "quiz")
    Set<QuizSection> sectionSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
