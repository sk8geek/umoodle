/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.forum

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="forum_posts")
class ForumPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "created", columnDefinition = "bigint(10)", nullable = false)
    Long created = 0

    @Column(name = "modified", columnDefinition = "bigint(10)", nullable = false)
    Long modified = 0

    @Column(name = "mailed", columnDefinition = "tinyint(2)", nullable = false)
    Integer mailed = 0

    @Column(name = "subject", length = 255, nullable = false)
    String subject

    @Column(name = "message", columnDefinition = "longtext", nullable = false)
    String message

    @Column(name = "messageformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer messageformat = 0

    @Column(name = "messagetrust", columnDefinition = "tinyint(2)", nullable = false)
    Integer messagetrust = 0

    @Column(name = "attachment", length = 100, nullable = false)
    String attachment

    @Column(name = "totalscore", columnDefinition = "smallint(4)", nullable = false)
    Integer totalscore = 0

    @Column(name = "mailnow", columnDefinition = "bigint(10)", nullable = false)
    Long mailnow = 0

    @Column(name = "deleted", columnDefinition = "bit(1)", nullable = false)
    Boolean deleted = false


    @ManyToOne()
    @JoinColumn(name = "parent", columnDefinition = "bigint(10)", nullable = false)
    Forum parent

    @OneToOne()
    @JoinColumn(name = "discussion", columnDefinition = "bigint(10)", nullable = false)
    ForumDiscussion discussion

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

//        <KEY NAME="parent" TYPE="foreign" FIELDS="parent" REFTABLE="forum_posts" REFFIELDS="id" COMMENT="note that to make this recursive FK working someday, the parent field must be decalred NULL"/>
}
