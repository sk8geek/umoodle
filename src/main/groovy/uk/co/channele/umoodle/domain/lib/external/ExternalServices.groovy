/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.external

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "external_services")
class ExternalServices {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 200, nullable = false)
    String name = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer enabled

    @Column(length = 150)
    String requiredcapability = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer restrictedusers

    @Column(length = 100)
    String component = ""

    @Column(nullable = false)
    Long timecreated

    @Column
    Long timemodified

    @Column(length = 255)
    String shortname = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer downloadfiles = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer uploadfiles = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="name" UNIQUE="true" FIELDS="name"/>
}
