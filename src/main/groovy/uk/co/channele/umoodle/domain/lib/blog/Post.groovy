/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.blog

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "post")
class Post {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 20, nullable = false)
    String module = ""

    @Column(nullable = false)
    Long userid = 0

    @Column(nullable = false)
    Long courseid = 0

    @Column(nullable = false)
    Long groupid = 0

    @Column(nullable = false)
    Long moduleid = 0

    @Column(nullable = false)
    Long coursemoduleid = 0

    @Column(length = 128, nullable = false)
    String subject = ""

    @Column(columnDefinition = "longtext")
    String summary = ""

    @Column(columnDefinition = "longtext")
    String content = ""

    @Column(length = 255, nullable = false)
    String uniquehash = ""

    @Column(nullable = false)
    Long rating = 0

    @Column(nullable = false)
    Long format = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer summaryformat = 0

    @Column(length = 100)
    String attachment = ""

    @Column(length = 20, nullable = false)
    String publishstate = "draft"

    @Column(nullable = false)
    Long lastmodified = 0

    @Column(nullable = false)
    Long created = 0

    @Column
    Long usermodified


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="usermodified" TYPE="foreign" FIELDS="usermodified" REFTABLE="user" REFFIELDS="id"/>

// <INDEX NAME="id-userid" UNIQUE="true" FIELDS="id, userid"/>
// <INDEX NAME="lastmodified" UNIQUE="false" FIELDS="lastmodified"/>
// <INDEX NAME="module" UNIQUE="false" FIELDS="module"/>
// <INDEX NAME="subject" UNIQUE="false" FIELDS="subject"/>
}
