/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.grade

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "grade_outcomes", uniqueConstraints = @UniqueConstraint(columnNames = ["courseid", "shortname"]))
class GradeOutcome {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String shortname = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String fullname = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer descriptionformat = 0

    @Column(name = "timecreated")
    Long timeCreated

    @Column(name = "timemodified")
    Long timeModified


    @OneToMany(mappedBy = "gradeOutcome")
    Set<GradeItem> gradeItemSet

    @OneToMany(mappedBy = "gradeOutcome")
    Set<GradeOutcomesHistory> gradeOutcomesHistorySet

    @ManyToOne
    @JoinColumn(name = "courseid", nullable = false)
    Course course

    @OneToOne
    @JoinColumn(name = "scaleid")
    Scale scale

    @OneToOne
    @JoinColumn(name = "usermodified", nullable = false)
    User userModified

}
