/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.enrol.lti

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="enrol_lti_lti2_tool_proxy")
class EnrolLtiLti2ToolProxy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "toolproxykey", length = 32, nullable = false, unique = true)
    String toolproxykey

    @Column(name = "toolproxy", columnDefinition = "longtext", nullable = false)
    String toolproxy

    @Column(name = "created", columnDefinition = "bigint(10)", nullable = false)
    Long created

    @Column(name = "updated", columnDefinition = "bigint(10)", nullable = false)
    Long updated


    @OneToOne()
    @JoinColumn(name = "consumerid", columnDefinition = "bigint(11)", nullable = false)
    EnrolLtiLti2Consumer lti2Consumer

}
