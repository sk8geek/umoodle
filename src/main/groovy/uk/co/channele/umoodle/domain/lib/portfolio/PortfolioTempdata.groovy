/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.portfolio

import uk.co.channele.umoodle.domain.lib.user.User
import uk.co.channele.umoodle.domain.portfolio.mahara.PortfolioMaharaQueue
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "portfolio_tempdata")
class PortfolioTempdata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "longtext")
    String data = ""

    @Column(nullable = false)
    Long expirytime

    @Column(columnDefinition = "bit(1)", nullable = false)
    Boolean queued = false


    @OneToMany(mappedBy = "transfer")
    Set<PortfolioMaharaQueue> portfolioMaharaQueueSet

    @OneToOne()
    @JoinColumn(name = "userid", nullable = false)
    User user

    @ManyToOne
    @JoinColumn(name = "instance")
    PortfolioInstance portfolioInstance

}