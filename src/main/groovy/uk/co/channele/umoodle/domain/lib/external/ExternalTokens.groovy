/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.external

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "external_tokens")
class ExternalTokens {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 128, nullable = false)
    String token = ""

    @Column(length = 64)
    String privatetoken = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer tokentype

    @Column(nullable = false)
    Long userid

    @Column(nullable = false)
    Long externalserviceid

    @Column(length = 128)
    String sid = ""

    @Column(nullable = false)
    Long contextid

    @Column(nullable = false)
    Long creatorid = 1

    @Column(length = 255)
    String iprestriction = ""

    @Column
    Long validuntil

    @Column(nullable = false)
    Long timecreated

    @Column
    Long lastaccess


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="userid" TYPE="foreign" FIELDS="userid" REFTABLE="user" REFFIELDS="id"/>
// <KEY NAME="externalserviceid" TYPE="foreign" FIELDS="externalserviceid" REFTABLE="external_services" REFFIELDS="id"/>
// <KEY NAME="contextid" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>
// <KEY NAME="creatorid" TYPE="foreign" FIELDS="creatorid" REFTABLE="user" REFFIELDS="id"/>
}
