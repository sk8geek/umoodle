/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.monitor

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="tool_monitor_rules")
class ToolMonitorRule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "bit(1)", nullable = false)
    Boolean descriptionFormat

    @Column(name = "name", length = 254, nullable = false)
    String name

    @Column(name = "plugin", length = 254, nullable = false)
    String plugin

    @Column(name = "eventname", length = 254, nullable = false)
    String eventName

    @Column(name = "template", columnDefinition = "longtext", nullable = false)
    String template

    @Column(name = "templateformat", columnDefinition = "bit(1)", nullable = false)
    Boolean templateFormat

    @Column(name = "frequency", columnDefinition = "smallint(4)", nullable = false)
    Integer frequency

    @Column(name = "timewindow", columnDefinition = "smallint(5)", nullable = false)
    Integer timeWindow

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated


    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "courseid", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
