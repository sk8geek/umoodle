/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.grade

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "grade_categories_history")
class GradeCategoriesHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long action = 0

    @Column(length = 255)
    String source = ""

    @Column(name = "timemodified")
    Long timeModified

    @Column(nullable = false)
    Long depth = 0

    @Column(length = 255)
    String path = ""

    @Column(name = "fullname", length = 255, nullable = false)
    String fullName = ""

    @Column(nullable = false)
    Long aggregation = 0

    @Column(name = "keephigh", nullable = false)
    Long keepHigh = 0

    @Column(name = "droplow", nullable = false)
    Long dropLow = 0

    @Column(name = "aggregateonlygraded", columnDefinition = "bit(1)", nullable = false)
    Integer aggregateOnlyGraded = 0

    @Column(name = "aggregateoutcomes", columnDefinition = "bit(1)", nullable = false)
    Integer aggregateOutcomes = 0

    @Column(name = "aggregatesubcats", columnDefinition = "bit(1)", nullable = false)
    Integer aggregateSubCats = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer hidden = 0

    @ManyToOne
    @JoinColumn(name = "courseid", nullable = false)
    Course course

    @OneToOne
    @JoinColumn(name = "parent")
    GradeCategory parent

    @OneToOne
    @JoinColumn(name = "oldid", nullable = false)
    GradeCategory oldId

    @OneToOne
    @JoinColumn(name = "loggeduser")
    User loggedUser

}
