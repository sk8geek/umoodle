/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.blocks

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="block_rss_client")
class BlockRssClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "title", columnDefinition = "longtext", nullable = false)
    String title

    @Column(name = "preferredtitle", length = 64, nullable = false)
    String preferredTitle

    @Column(name = "description", columnDefinition = "longtext", nullable = false)
    String description

    @Column(name = "shared", columnDefinition = "tinyint(2)", nullable = false)
    Integer shared = 0

    @Column(name = "url", length = 255, nullable = false)
    String url

    @Column(name = "skiptime", columnDefinition = "bigint(10)", nullable = false)
    Long skipTime = 0

    @Column(name = "skipuntil", columnDefinition = "bigint(10)", nullable = false)
    Long skipUntil = 0


    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
