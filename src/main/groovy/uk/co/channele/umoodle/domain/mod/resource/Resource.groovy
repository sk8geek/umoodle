/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.resource

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="resource")
class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext")
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "tobemigrated", columnDefinition = "smallint(4)", nullable = false)
    Integer tobemigrated = 0

    @Column(name = "legacyfiles", columnDefinition = "smallint(4)", nullable = false)
    Integer legacyfiles = 0

    @Column(name = "legacyfileslast", columnDefinition = "bigint(10)")
    Long legacyfileslast

    @Column(name = "display", columnDefinition = "smallint(4)", nullable = false)
    Integer display = 0

    @Column(name = "displayoptions", columnDefinition = "longtext")
    String displayoptions

    @Column(name = "filterfiles", columnDefinition = "smallint(4)", nullable = false)
    Integer filterfiles = 0

    @Column(name = "revision", columnDefinition = "bigint(10)", nullable = false)
    Long revision = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
