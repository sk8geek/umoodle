/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.enrol.lti

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.course.Enrol

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="enrol_lti_tools")
class EnrolLtiTool {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "institution", length = 40, nullable = false)
    String institution

    @Column(name = "lang", length = 30, nullable = false)
    String lang

    @Column(name = "timezone", length = 100, nullable = false)
    String timezone

    @Column(name = "maxenrolled", columnDefinition = "bigint(10)", nullable = false)
    Long maxenrolled = 0

    @Column(name = "maildisplay", columnDefinition = "tinyint(2)", nullable = false)
    Integer maildisplay = 2

    @Column(name = "city", length = 120, nullable = false)
    String city

    @Column(name = "country", length = 2, nullable = false)
    String country

    @Column(name = "gradesync", columnDefinition = "bit(1)", nullable = false)
    Boolean gradesync = false

    @Column(name = "gradesynccompletion", columnDefinition = "bit(1)", nullable = false)
    Boolean gradesynccompletion = false

    @Column(name = "membersync", columnDefinition = "bit(1)", nullable = false)
    Boolean membersync = false

    @Column(name = "membersyncmode", columnDefinition = "bit(1)", nullable = false)
    Boolean membersyncmode = false

    @Column(name = "roleinstructor", columnDefinition = "bigint(10)", nullable = false)
    Long roleinstructor

    @Column(name = "rolelearner", columnDefinition = "bigint(10)", nullable = false)
    Long rolelearner

    @Column(name = "secret", columnDefinition = "longtext")
    String secret

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified


    @OneToOne()
    @JoinColumn(name = "enrolid", columnDefinition = "bigint(10)", nullable = false)
    Enrol enrol

    @OneToOne()
    @JoinColumn(name = "contextid", columnDefinition = "bigint(10)", nullable = false)
    Context context

}
