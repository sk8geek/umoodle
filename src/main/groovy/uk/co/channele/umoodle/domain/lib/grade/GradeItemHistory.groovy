/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.grade

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "grade_items_history")
class GradeItemHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long action = 0

    @Column(length = 255)
    String source = ""

    @Column(name = "timemodified")
    Long timeModified

    @Column(length = 255)
    String itemname = ""

    @Column(length = 30, nullable = false)
    String itemtype = ""

    @Column(length = 30)
    String itemmodule = ""

    @Column()
    Long iteminstance

    @Column()
    Long itemnumber

    @Column(columnDefinition = "longtext")
    String iteminfo = ""

    @Column(length = 255)
    String idnumber = ""

    @Column(columnDefinition = "longtext")
    String calculation = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer gradetype = 1

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grademax

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grademin

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal gradepass

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal multfactor

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal plusfactor

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal aggregationcoef

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal aggregationcoef2

    @Column(nullable = false)
    Long sortorder = 0

    @Column(nullable = false)
    Long hidden = 0

    @Column(nullable = false)
    Long locked = 0

    @Column(nullable = false)
    Long locktime = 0

    @Column(nullable = false)
    Long needsupdate = 0

    @Column(nullable = false)
    Long display = 0

    @Column(columnDefinition = "bit(1)")
    Integer decimals

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer weightoverride = 0


    @ManyToOne
    @JoinColumn(name = "courseid", nullable = false)
    Course course

    @ManyToOne
    @JoinColumn(name = "categoryid")
    GradeCategory gradeCategory

    @OneToOne
    @JoinColumn(name = "outcomeid")
    GradeOutcome outcomeid

    @OneToOne
    @JoinColumn(name = "oldid", nullable = false)
    GradeItem oldid

    @OneToOne
    @JoinColumn(name = "scaleid")
    Scale scale

    @OneToOne
    @JoinColumn(name = "loggeduser")
    User loggedUser

}
