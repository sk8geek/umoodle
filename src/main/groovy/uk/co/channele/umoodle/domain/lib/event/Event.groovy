/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.event

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "event")
class Event {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "longtext", nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String description = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer format = 0

    @Column(nullable = false)
    Long categoryid = 0

    @Column(nullable = false)
    Long courseid = 0

    @Column(nullable = false)
    Long groupid = 0

    @Column(nullable = false)
    Long userid = 0

    @Column(nullable = false)
    Long repeatid = 0

    @Column(length = 20, nullable = false)
    String modulename = ""

    @Column(nullable = false)
    Long instance = 0

    @Column(columnDefinition = "smallint", nullable = false)
    Integer type = 0

    @Column(length = 20, nullable = false)
    String eventtype = ""

    @Column(nullable = false)
    Long timestart = 0

    @Column(nullable = false)
    Long timeduration = 0

    @Column
    Long timesort

    @Column(columnDefinition = "smallint", nullable = false)
    Integer visible = 1

    @Column(length = 255, nullable = false)
    String uuid = ""

    @Column(nullable = false)
    Long sequence = 1

    @Column(nullable = false)
    Long timemodified = 0

    @Column
    Long priority

    @Column(columnDefinition = "longtext")
    String location = ""


    @ManyToOne
    @JoinColumn(name = "subscriptionid")
    EventSubscription eventSubscription


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="categoryid" TYPE="foreign" FIELDS="categoryid" REFTABLE="course_categories" REFFIELDS="id"/>
// <KEY NAME="subscriptionid" TYPE="foreign" FIELDS="subscriptionid" REFTABLE="event_subscriptions" REFFIELDS="id"/>

// <INDEX NAME="courseid" UNIQUE="false" FIELDS="courseid"/>
// <INDEX NAME="userid" UNIQUE="false" FIELDS="userid"/>
// <INDEX NAME="timestart" UNIQUE="false" FIELDS="timestart"/>
// <INDEX NAME="timeduration" UNIQUE="false" FIELDS="timeduration"/>
// <INDEX NAME="uuid" UNIQUE="false" FIELDS="uuid"/>
// <INDEX NAME="type-timesort" UNIQUE="false" FIELDS="type, timesort"/>
// <INDEX NAME="groupid-courseid-categoryid-visible-userid" UNIQUE="false" FIELDS="groupid, courseid, categoryid, visible, userid" COMMENT="used for calendar view"/>
}
