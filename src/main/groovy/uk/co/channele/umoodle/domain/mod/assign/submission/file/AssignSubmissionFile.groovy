/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assign.submission.file

import uk.co.channele.umoodle.domain.mod.assign.Assign
import uk.co.channele.umoodle.domain.mod.assign.AssignSubmission

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assignsubmission_file")
class AssignSubmissionFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "numfiles", columnDefinition = "bigint(10)", nullable = false)
    Long numfiles = 0


    @OneToOne()
    @JoinColumn(name = "assignment", columnDefinition = "bigint(10)", nullable = false)
    Assign assignment

    @OneToOne()
    @JoinColumn(name = "submission", columnDefinition = "bigint(10)", nullable = false)
    AssignSubmission submission

}
