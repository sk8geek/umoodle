/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assign

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assign")
class Assign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "alwaysshowdescription", columnDefinition = "tinyint(2)", nullable = false)
    Integer alwaysshowdescription = 0

    @Column(name = "nosubmissions", columnDefinition = "tinyint(2)", nullable = false)
    Integer nosubmissions = 0

    @Column(name = "submissiondrafts", columnDefinition = "tinyint(2)", nullable = false)
    Integer submissiondrafts = 0

    @Column(name = "sendnotifications", columnDefinition = "tinyint(2)", nullable = false)
    Integer sendnotifications = 0

    @Column(name = "sendlatenotifications", columnDefinition = "tinyint(2)", nullable = false)
    Integer sendlatenotifications = 0

    @Column(name = "duedate", columnDefinition = "bigint(10)", nullable = false)
    Long duedate = 0

    @Column(name = "allowsubmissionsfromdate", columnDefinition = "bigint(10)", nullable = false)
    Long allowsubmissionsfromdate = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "requiresubmissionstatement", columnDefinition = "tinyint(2)", nullable = false)
    Integer requiresubmissionstatement = 0

    @Column(name = "completionsubmit", columnDefinition = "tinyint(2)", nullable = false)
    Integer completionsubmit = 0

    @Column(name = "cutoffdate", columnDefinition = "bigint(10)", nullable = false)
    Long cutoffdate = 0

    @Column(name = "gradingduedate", columnDefinition = "bigint(10)", nullable = false)
    Long gradingduedate = 0

    @Column(name = "teamsubmission", columnDefinition = "tinyint(2)", nullable = false)
    Integer teamsubmission = 0

    @Column(name = "requireallteammemberssubmit", columnDefinition = "tinyint(2)", nullable = false)
    Integer requireallteammemberssubmit = 0

    @Column(name = "teamsubmissiongroupingid", columnDefinition = "bigint(10)", nullable = false)
    Long teamsubmissiongroupingid = 0

    @Column(name = "blindmarking", columnDefinition = "tinyint(2)", nullable = false)
    Integer blindmarking = 0

    // TODO: Moodle 3.8
//    @Column(name = "hidegrader", columnDefinition = "tinyint(2)", nullable = false)
//    Integer hidegrader = 0

    @Column(name = "revealidentities", columnDefinition = "tinyint(2)", nullable = false)
    Integer revealidentities = 0

    @Column(name = "attemptreopenmethod", length = 10, nullable = false)
    String attemptreopenmethod

    @Column(name = "maxattempts", columnDefinition = "mediumint(6)", nullable = false)
    Integer maxattempts = -1

    @Column(name = "markingworkflow", columnDefinition = "tinyint(2)", nullable = false)
    Integer markingworkflow = 0

    @Column(name = "markingallocation", columnDefinition = "tinyint(2)", nullable = false)
    Integer markingallocation = 0

    @Column(name = "sendstudentnotifications", columnDefinition = "tinyint(2)", nullable = false)
    Integer sendstudentnotifications = 1

    @Column(name = "preventsubmissionnotingroup", columnDefinition = "tinyint(2)", nullable = false)
    Integer preventsubmissionnotingroup = 0


    @OneToMany(mappedBy = "assignment")
    Set<AssignGrade> gradeSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
