/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="logstore_standard_log")
class LogStoreStandardLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "eventname", length = 255, nullable = false)
    String eventName

    @Column(name = "component", length = 100, nullable = false)
    String component

    @Column(name = "action", length = 100, nullable = false)
    String action

    @Column(name = "target", length = 100, nullable = false)
    String target

    @Column(name = "objecttable", length = 50)
    String objectTable

    @Column(name = "objectid", columnDefinition = "bigint(10)")
    Long objectId

    @Column(name = "crud", length = 1, nullable = false)
    String crud

    @Column(name = "edulevel", columnDefinition = "bit(1)", nullable = false)
    Boolean eduLevel

    @Column(name = "contextlevel", columnDefinition = "bigint(10)", nullable = false)
    Long contextLevel

    @Column(name = "anonymous", columnDefinition = "bit(1)", nullable = false)
    Boolean anonymous = false

    @Column(name = "other", columnDefinition = "longtext")
    String other

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "origin", length = 10)
    String origin

    @Column(name = "ip", length = 45)
    String ip


    @OneToOne()
    @JoinColumn(name = "realuserid", columnDefinition = "bigint(10)")
    User realUser

    @OneToOne()
    @JoinColumn(name = "contextid", columnDefinition = "bigint(10)", nullable = false)
    Context contextId

    @OneToOne()
    @JoinColumn(name = "contextinstanceid", columnDefinition = "bigint(10)", nullable = false)
    Context contextInstanced

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "courseid", columnDefinition = "bigint(10)")
    Course course

    @OneToOne()
    @JoinColumn(name = "relateduserid", columnDefinition = "bigint(10)")
    User relatedUser

}
