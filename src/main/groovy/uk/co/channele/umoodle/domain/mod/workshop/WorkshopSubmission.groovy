/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.workshop

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="workshop_submissions")
class WorkshopSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "example", columnDefinition = "tinyint(2)")
    Integer example = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "title", length = 255, nullable = false)
    String title

    @Column(name = "content", columnDefinition = "longtext")
    String content

    @Column(name = "contentformat", columnDefinition = "smallint(3)", nullable = false)
    Integer contentformat = 0

    @Column(name = "contenttrust", columnDefinition = "smallint(3)", nullable = false)
    Integer contenttrust = 0

    @Column(name = "attachment", columnDefinition = "tinyint(2)")
    Integer attachment = 0

    @Column(name = "grade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grade

    @Column(name = "gradeover", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal gradeover

    @Column(name = "feedbackauthor", columnDefinition = "longtext")
    String feedbackauthor

    @Column(name = "feedbackauthorformat", columnDefinition = "smallint(3)")
    Integer feedbackauthorformat = 0

    @Column(name = "timegraded", columnDefinition = "bigint(10)")
    Long timegraded

    @Column(name = "published", columnDefinition = "tinyint(2)")
    Integer published = 0

    @Column(name = "late", columnDefinition = "tinyint(2)", nullable = false)
    Integer late = 0


    @ManyToOne()
    @JoinColumn(name = "workshopid", columnDefinition = "bigint(10)", nullable = false)
    Workshop workshop

    @OneToOne()
    @JoinColumn(name = "authorid", columnDefinition = "bigint(10)", nullable = false)
    User author

    @OneToOne()
    @JoinColumn(name = "gradeoverby", columnDefinition = "bigint(10)")
    User gradeOverBy

}
