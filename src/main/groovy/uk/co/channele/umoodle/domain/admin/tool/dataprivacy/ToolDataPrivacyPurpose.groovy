/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.dataprivacy

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="tool_dataprivacy_purpose")
class ToolDataPrivacyPurpose {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 100, nullable = false)
    String name

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "bit(1)")
    Boolean descriptionFormat

    @Column(name = "lawfulbases", columnDefinition = "longtext", nullable = false)
    String lawfulBases

    @Column(name = "sensitivedatareasons", columnDefinition = "longtext")
    String sensitiveDataReasons

    @Column(name = "retentionperiod", length = 255, nullable = false)
    String retentionPeriod

    @Column(name = "protected", columnDefinition = "bit(1)")
    Boolean privacyProtected

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "purpose")
    Set<ToolDataPrivacyContextInstance> contextInstanceSet

    @OneToMany(mappedBy = "purpose")
    Set<ToolDataPrivacyContextLevel> contextLevelSet

    @OneToMany(mappedBy = "purpose")
    Set<ToolDataPrivacyPurposeRole> roleSet

    @OneToOne()
    @JoinColumn(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    User modifiedBy

}
