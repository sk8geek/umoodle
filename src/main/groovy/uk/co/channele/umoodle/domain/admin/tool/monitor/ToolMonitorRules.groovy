package uk.co.channele.umoodle.domain.admin.tool.monitor

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="tool_monitor_rules")
class ToolMonitorRules {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "bit(1)", nullable = false)
    Boolean descriptionformat

    @Column(name = "name", length = 254, nullable = false)
    String name

    @Column(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    Long userid

    @Column(name = "courseid", columnDefinition = "bigint(10)", nullable = false)
    Long courseid

    @Column(name = "plugin", length = 254, nullable = false)
    String plugin

    @Column(name = "eventname", length = 254, nullable = false)
    String eventname

    @Column(name = "template", columnDefinition = "longtext", nullable = false)
    String template

    @Column(name = "templateformat", columnDefinition = "bit(1)", nullable = false)
    Boolean templateformat

    @Column(name = "frequency", columnDefinition = "smallint(4)", nullable = false)
    Integer frequency

    @Column(name = "timewindow", columnDefinition = "smallint(5)", nullable = false)
    Integer timewindow

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated


//        <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

//        <INDEX NAME="courseanduser" UNIQUE="false" FIELDS="courseid, userid" COMMENT="Index on courseid and userid"/>
//        <INDEX NAME="eventname" UNIQUE="false" FIELDS="eventname" COMMENT="eventname"/>

}
