/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import uk.co.channele.umoodle.domain.admin.tool.monitor.ToolMonitorEvent
import uk.co.channele.umoodle.domain.blocks.BlockRecentActivity
import uk.co.channele.umoodle.domain.lib.grade.GradeSetting
import uk.co.channele.umoodle.domain.lib.user.Log
import uk.co.channele.umoodle.domain.mod.chat.Chat
import uk.co.channele.umoodle.domain.mod.feedback.Feedback
import uk.co.channele.umoodle.domain.mod.feedback.FeedbackCompleted
import uk.co.channele.umoodle.domain.mod.glossary.Glossary
import uk.co.channele.umoodle.domain.mod.lesson.Lesson

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "course")
@JsonIgnoreProperties(["blockRecentActivitySet", "chatSet", "completionAggrMethodSet", "completionCritComplSet", "courseCompletionCriteriaSet",
        "courseCompletionDefaultsSet", "courseCompletionsSet", "courseFormatOptionSet", "modules",
        "coursePublishedSet", "courseSectionSet", "enrolSet", "feedbackSet", "feedbackCompletedSet",
        "glossarySet", "gradeSettingSet", "lessonSet", "logSet", "monitorEventSet"])
class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "sortorder", nullable = false)
    Long sortOrder = 0

    @Column(name = "fullname", length = 254, nullable = false)
    String fullName = ""

    @Column(name = "shortname", length = 255, nullable = false)
    String shortName = ""

    @Column(name = "idnumber", length = 100, nullable = false)
    String idNumber = ""

    @Column(name = "summary", columnDefinition = "longtext")
    String summary = ""

    @Column(name = "summaryformat", columnDefinition = "tinyint", nullable = false)
    Integer summaryFormat = 0

    @Column(name = "format", length = 21, nullable = false)
    String format = "topics"

    @Column(name = "showgrades", columnDefinition = "tinyint", nullable = false)
    Integer showGrades = 1

    @Column(name = "newsitems", columnDefinition = "mediumint", nullable = false)
    Integer newsItems = 1

    @Column(name = "startdate", nullable = false)
    Long startDate = 0

    @Column(name = "enddate", nullable = false)
    Long endDate = 0

    @Column(name = "marker", nullable = false)
    Long marker = 0

    @Column(name = "maxbytes", nullable = false)
    Long maxBytes = 0

    @Column(name = "legacyfiles", columnDefinition = "smallint", nullable = false)
    Integer legacyFiles = 0

    @Column(name = "showreports", columnDefinition = "smallint", nullable = false)
    Integer showReports = 0

    @Column(name = "visible", columnDefinition = "bit(1)", nullable = false)
    Integer visible = 1

    @Column(name = "visibleold", columnDefinition = "bit(1)", nullable = false)
    Integer visibleOld = 1

    @Column(name = "groupmode", columnDefinition = "smallint", nullable = false)
    Integer groupMode = 0

    @Column(name = "groupmodeforce", columnDefinition = "smallint", nullable = false)
    Integer groupModeForce = 0

    @Column(name = "defaultgroupingid", nullable = false)
    Long defaultGroupingId = 0

    @Column(name = "lang", length = 30, nullable = false)
    String lang = ""

    @Column(name = "calendartype", length = 30, nullable = false)
    String calendarType = ""

    @Column(name = "theme", length = 50, nullable = false)
    String theme = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", nullable = false)
    Long timeModified = 0

    @Column(name = "requested", columnDefinition = "bit(1)", nullable = false)
    Integer requested = 0

    @Column(name = "enablecompletion", columnDefinition = "bit(1)", nullable = false)
    Integer enableCompletion = 0

    @Column(name = "completionnotify", columnDefinition = "bit(1)", nullable = false)
    Integer completionNotify = 0

    @Column(name = "cacherev", nullable = false)
    Long cacheRev = 0


    @OneToMany(mappedBy = "course")
    Set<BlockRecentActivity> blockRecentActivitySet

    @OneToMany(mappedBy = "course")
    Set<Chat> chatSet

    @OneToMany(mappedBy = "course")
    Set<CourseCompletionAggrMethod> completionAggrMethodSet

    @OneToMany(mappedBy = "course")
    Set<CourseCompletionCritCompl> completionCritComplSet

    @OneToMany(mappedBy = "course")
    Set<CourseCompletionCriteria> courseCompletionCriteriaSet

    @OneToMany(mappedBy = "course")
    Set<CourseCompletionDefaults> courseCompletionDefaultsSet

    @OneToMany(mappedBy = "course")
    Set<CourseCompletions> courseCompletionsSet

    @OneToMany(mappedBy = "course")
    Set<CourseFormatOption> courseFormatOptionSet

    @OneToMany(mappedBy = "course")
    Set<CoursePublished> coursePublishedSet

    @OneToMany(mappedBy = "course")
    Set<CourseSection> courseSectionSet

    @OneToMany(mappedBy = "course")
    Set<Enrol> enrolSet

    @OneToMany(mappedBy = "course")
    Set<Feedback> feedbackSet

    @OneToMany(mappedBy = "course")
    Set<FeedbackCompleted> feedbackCompletedSet

    @OneToMany(mappedBy = "course")
    Set<Glossary> glossarySet

    @OneToMany(mappedBy = "course")
    Set<GradeSetting> gradeSettingSet

    @OneToMany(mappedBy = "course")
    Set<Lesson> lessonSet

    @OneToMany(mappedBy = "course")
    Set<Log> logSet

    @OneToMany(mappedBy = "course")
    Set<CourseModule> modules

    @OneToMany(mappedBy = "course")
    Set<ToolMonitorEvent> monitorEventSet


    @ManyToOne
    @JoinColumn(name = "category", nullable = false)
    CourseCategory category

}
