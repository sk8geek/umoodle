/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.file

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "files")
class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 40, nullable = false)
    String contenthash = ""

    @Column(length = 40, nullable = false, unique = true)
    String pathnamehash = ""

    @Column(length = 100, nullable = false)
    String component = ""

    @Column(length = 50, nullable = false)
    String filearea = ""

    @Column(nullable = false)
    Long itemid

    @Column(length = 255, nullable = false)
    String filepath = ""

    @Column(length = 255, nullable = false)
    String filename = ""

    @Column(nullable = false)
    Long filesize

    @Column(length = 100)
    String mimetype = ""

    @Column(nullable = false)
    Long status = 0

    @Column(columnDefinition = "longtext")
    String source = ""

    @Column(length = 255)
    String author = ""

    @Column(length = 255)
    String license = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(nullable = false)
    Long sortorder = 0

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @ManyToOne
    @JoinColumn(name = "contextid", nullable = false)
    Context context

    @ManyToOne
    @JoinColumn(name = "referencefileid")
    FilesReference referenceFile

    @OneToOne
    @JoinColumn(name = "userid")
    User user

}
