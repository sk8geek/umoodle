/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.quiz

import uk.co.channele.umoodle.domain.lib.question.Question
import uk.co.channele.umoodle.domain.lib.question.QuestionCategory

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="quiz_slots", uniqueConstraints = @UniqueConstraint(columnNames = ["quizid", "slot"]))
class QuizSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "slot", columnDefinition = "bigint(10)", nullable = false)
    Long slot

    @Column(name = "page", columnDefinition = "bigint(10)", nullable = false)
    Long page

    @Column(name = "requireprevious", columnDefinition = "smallint(4)", nullable = false)
    Integer requireprevious = 0

    @Column(name = "includingsubcategories", columnDefinition = "smallint(4)")
    Integer includingsubcategories

    @Column(name = "maxmark", columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal maxmark = 0


    @OneToOne()
    @JoinColumn(name = "quizid", columnDefinition = "bigint(10)", nullable = false)
    Quiz quiz

    @OneToOne()
    @JoinColumn(name = "questionid", columnDefinition = "bigint(10)", nullable = false)
    Question question

    @OneToOne()
    @JoinColumn(name = "questioncategoryid", columnDefinition = "bigint(10)")
    QuestionCategory questionCategory

}
