/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assignment

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assignment")
class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "assignmenttype", length = 50, nullable = false)
    String assignmenttype

    @Column(name = "resubmit", columnDefinition = "tinyint(2)", nullable = false)
    Integer resubmit = 0

    @Column(name = "preventlate", columnDefinition = "tinyint(2)", nullable = false)
    Integer preventlate = 0

    @Column(name = "emailteachers", columnDefinition = "tinyint(2)", nullable = false)
    Integer emailteachers = 0

    @Column(name = "var1", columnDefinition = "bigint(10)")
    Long var1 = 0

    @Column(name = "var2", columnDefinition = "bigint(10)")
    Long var2 = 0

    @Column(name = "var3", columnDefinition = "bigint(10)")
    Long var3 = 0

    @Column(name = "var4", columnDefinition = "bigint(10)")
    Long var4 = 0

    @Column(name = "var5", columnDefinition = "bigint(10)")
    Long var5 = 0

    @Column(name = "maxbytes", columnDefinition = "bigint(10)", nullable = false)
    Long maxbytes = 100000

    @Column(name = "timedue", columnDefinition = "bigint(10)", nullable = false)
    Long timedue = 0

    @Column(name = "timeavailable", columnDefinition = "bigint(10)", nullable = false)
    Long timeavailable = 0

    @Column(name = "grade", columnDefinition = "bigint(10)", nullable = false)
    Long grade = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
