/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.mnet

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "mnet_host2service")
class MnetHost2service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long hostid = 0

    @Column(nullable = false)
    Long serviceid = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer publish = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer subscribe = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id" COMMENT="primary key of the mnet_host2service table"/>

// <INDEX NAME="hostid_serviceid" UNIQUE="true" FIELDS="hostid, serviceid"/>
}