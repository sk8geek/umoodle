/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.grade.grading.form.guide

import uk.co.channele.umoodle.domain.lib.context.GradingInstance
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="gradingform_guide_fillings", uniqueConstraints = @UniqueConstraint(columnNames = ["instanceid", "criterionid"]))
class GradingFormGuideFillings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "remark", columnDefinition = "longtext")
    String remark

    @Column(name = "remarkformat", columnDefinition = "tinyint(2)")
    Integer remarkFormat

    @Column(name = "score", columnDefinition = "decimal(10,5)")
    BigDecimal score


    @OneToOne()
    @JoinColumn(name = "instanceid", columnDefinition = "bigint(10)", nullable = false)
    GradingInstance instance

    @OneToOne()
    @JoinColumn(name = "criterionid", columnDefinition = "bigint(10)", nullable = false)
    GradingFormGuideCriteria criterion

}
