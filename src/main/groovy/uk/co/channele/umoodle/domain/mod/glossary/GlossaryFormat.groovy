/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.glossary

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="glossary_formats")
class GlossaryFormat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 50, nullable = false)
    String name

    @Column(name = "popupformatname", length = 50, nullable = false)
    String popupFormatName

    @Column(name = "visible", columnDefinition = "tinyint(2)", nullable = false)
    Integer visible = 1

    @Column(name = "showgroup", columnDefinition = "tinyint(2)", nullable = false)
    Integer showGroup = 1

    @Column(name = "showtabs", length = 100)
    String showTabs

    @Column(name = "defaultmode", length = 50, nullable = false)
    String defaultMode

    @Column(name = "defaulthook", length = 50, nullable = false)
    String defaultHook

    @Column(name = "sortkey", length = 50, nullable = false)
    String sortKey

    @Column(name = "sortorder", length = 50, nullable = false)
    String sortOrder

}
