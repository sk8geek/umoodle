/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.search.engine.simpledb

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="search_simpledb_index")
class SearchSimpleDbIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "docid", length = 255, nullable = false, unique = true)
    String docId

    @Column(name = "itemid", columnDefinition = "bigint(10)", nullable = false)
    Long itemId

    @Column(name = "title", columnDefinition = "longtext")
    String title

    @Column(name = "content", columnDefinition = "longtext")
    String content

    @Column(name = "areaid", length = 255, nullable = false)
    String areaId

    @Column(name = "type", columnDefinition = "bit(1)", nullable = false)
    Boolean type

    @Column(name = "modified", columnDefinition = "bigint(10)", nullable = false)
    Long modified

    @Column(name = "description1", columnDefinition = "longtext")
    String description1

    @Column(name = "description2", columnDefinition = "longtext")
    String description2


    @OneToOne()
    @JoinColumn(name = "contextid", columnDefinition = "bigint(10)", nullable = false)
    Context context

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)")
    User user

    @OneToOne
    @JoinColumn(name = "owneruserid", columnDefinition = "bigint(10)")
    User owner

    @OneToOne()
    @JoinColumn(name = "courseid", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
