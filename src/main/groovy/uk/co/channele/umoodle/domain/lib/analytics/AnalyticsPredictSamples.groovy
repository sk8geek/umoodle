/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.analytics

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "analytics_predict_samples")
class AnalyticsPredictSamples {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long analysableid

    @Column(length = 255, nullable = false)
    String timesplitting = ""

    @Column(nullable = false)
    Long rangeindex

    @Column(columnDefinition = "longtext", nullable = false)
    String sampleids = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @ManyToOne()
    @JoinColumn(name = "modelid", nullable = false)
    AnalyticsModel analyticsModel

}
