/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.message

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "message_conversations")
class MessageConversations {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long type = 1

    @Column(length = 255)
    String name = ""

    @Column(length = 40)
    String convhash = ""

    @Column(length = 100)
    String component = ""

    @Column(length = 100)
    String itemtype = ""

    @Column
    Long itemid

    @Column
    Long contextid

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer enabled = 0

    @Column(nullable = false)
    Long timecreated

    @Column
    Long timemodified


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="contextid" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>

// <INDEX NAME="type" UNIQUE="false" FIELDS="type"/>
// <INDEX NAME="convhash" UNIQUE="false" FIELDS="convhash"/>
// <INDEX NAME="component-itemtype-itemid-contextid" UNIQUE="false" FIELDS="component, itemtype, itemid, contextid"/>
}
