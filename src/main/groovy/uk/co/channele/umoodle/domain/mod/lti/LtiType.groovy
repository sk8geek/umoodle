/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lti

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="lti_types")
class LtiType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "baseurl", columnDefinition = "longtext", nullable = false)
    String baseurl

    @Column(name = "tooldomain", length = 255, nullable = false)
    String tooldomain

    @Column(name = "state", columnDefinition = "tinyint(2)", nullable = false)
    Integer state = 2

    @Column(name = "coursevisible", columnDefinition = "bit(1)", nullable = false)
    Boolean coursevisible = false

    @Column(name = "toolproxyid", columnDefinition = "bigint(10)")
    Long toolproxyid

    @Column(name = "enabledcapability", columnDefinition = "longtext")
    String enabledcapability

    @Column(name = "parameter", columnDefinition = "longtext")
    String parameter

    @Column(name = "icon", columnDefinition = "longtext")
    String icon

    @Column(name = "secureicon", columnDefinition = "longtext")
    String secureicon

    @Column(name = "createdby", columnDefinition = "bigint(10)", nullable = false)
    Long createdby

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "description", columnDefinition = "longtext")
    String description


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

//        <INDEX NAME="tooldomain" UNIQUE="false" FIELDS="tooldomain"/>
//        <INDEX NAME="clientid" UNIQUE="true" FIELDS="clientid"/>
}
