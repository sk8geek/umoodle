/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.monitor

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.course.Course
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="tool_monitor_events")
class ToolMonitorEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "eventname", length = 254, nullable = false)
    String eventName

    @Column(name = "link", length = 254, nullable = false)
    String link

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "contextlevel", columnDefinition = "bigint(10)", nullable = false)
    Long contextLevel


    @OneToOne()
    @JoinColumn(name = "contextid", columnDefinition = "bigint(10)", nullable = false)
    Context context

    @OneToOne()
    @JoinColumn(name = "contextinstanceid", columnDefinition = "bigint(10)", nullable = false)
    Context contextInstance

    @ManyToOne()
    @JoinColumn(name = "courseid", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
