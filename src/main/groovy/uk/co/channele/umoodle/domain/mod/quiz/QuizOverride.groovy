/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.quiz

import uk.co.channele.umoodle.domain.lib.group.Group
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="quiz_overrides")
class QuizOverride {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "timeopen", columnDefinition = "bigint(10)")
    Long timeOpen

    @Column(name = "timeclose", columnDefinition = "bigint(10)")
    Long timeClose

    @Column(name = "timelimit", columnDefinition = "bigint(10)")
    Long timeLimit

    @Column(name = "attempts", columnDefinition = "mediumint(6)")
    Integer attempts

    @Column(name = "password", length = 255)
    String password


    @OneToOne()
    @JoinColumn(name = "quiz", columnDefinition = "bigint(10)", nullable = false)
    Quiz quiz

    @OneToOne()
    @JoinColumn(name = "groupid", columnDefinition = "bigint(10)")
    Group group

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
