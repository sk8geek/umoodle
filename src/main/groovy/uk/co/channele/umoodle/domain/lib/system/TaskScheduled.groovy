/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "task_scheduled")
class TaskScheduled {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String component = ""

    @Column(length = 255, nullable = false)
    String classname = ""

    @Column
    Long lastruntime

    @Column
    Long nextruntime

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer blocking = 0

    @Column(length = 25, nullable = false)
    String minute = ""

    @Column(length = 25, nullable = false)
    String hour = ""

    @Column(length = 25, nullable = false)
    String day = ""

    @Column(length = 25, nullable = false)
    String month = ""

    @Column(length = 25, nullable = false)
    String dayofweek = ""

    @Column
    Long faildelay

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer customised = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer disabled = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="classname_uniq" UNIQUE="true" FIELDS="classname"/>
}
