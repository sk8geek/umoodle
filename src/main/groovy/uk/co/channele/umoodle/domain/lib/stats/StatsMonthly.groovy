/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.stats

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "stats_monthly")
class StatsMonthly {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long courseid = 0

    @Column(nullable = false)
    Long timeend = 0

    @Column(nullable = false)
    Long roleid = 0

    @Column(length = 20, nullable = false)
    String stattype = "activity"

    @Column(nullable = false)
    Long stat1 = 0

    @Column(nullable = false)
    Long stat2 = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="courseid" UNIQUE="false" FIELDS="courseid"/>
// <INDEX NAME="timeend" UNIQUE="false" FIELDS="timeend"/>
// <INDEX NAME="roleid" UNIQUE="false" FIELDS="roleid"/>
}
