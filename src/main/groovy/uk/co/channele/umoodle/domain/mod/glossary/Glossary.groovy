/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.glossary

import uk.co.channele.umoodle.domain.lib.course.Course
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name ="glossary")
class Glossary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introFormat = 0

    @Column(name = "allowduplicatedentries", columnDefinition = "tinyint(2)", nullable = false)
    Integer allowDuplicatedEntries = 0

    @Column(name = "displayformat", length = 50, nullable = false)
    String displayFormat

    @Column(name = "mainglossary", columnDefinition = "tinyint(2)", nullable = false)
    Integer mainGlossary = 0

    @Column(name = "showspecial", columnDefinition = "tinyint(2)", nullable = false)
    Integer showSpecial = 1

    @Column(name = "showalphabet", columnDefinition = "tinyint(2)", nullable = false)
    Integer showAlphabet = 1

    @Column(name = "showall", columnDefinition = "tinyint(2)", nullable = false)
    Integer showAll = 1

    @Column(name = "allowcomments", columnDefinition = "tinyint(2)", nullable = false)
    Integer allowcomments = 0

    @Column(name = "allowprintview", columnDefinition = "tinyint(2)", nullable = false)
    Integer allowPrintView = 1

    @Column(name = "usedynalink", columnDefinition = "tinyint(2)", nullable = false)
    Integer useDynaLink = 1

    @Column(name = "defaultapproval", columnDefinition = "tinyint(2)", nullable = false)
    Integer defaultApproval = 1

    @Column(name = "approvaldisplayformat", length = 50, nullable = false)
    String approvalDisplayFormat

    @Column(name = "globalglossary", columnDefinition = "tinyint(2)", nullable = false)
    Integer globalGlossary = 0

    @Column(name = "entbypage", columnDefinition = "smallint(3)", nullable = false)
    Integer entByPage = 10

    @Column(name = "editalways", columnDefinition = "tinyint(2)", nullable = false)
    Integer editAlways = 0

    @Column(name = "rsstype", columnDefinition = "tinyint(2)", nullable = false)
    Integer rssType = 0

    @Column(name = "rssarticles", columnDefinition = "tinyint(2)", nullable = false)
    Integer rssArticles = 0

    @Column(name = "assessed", columnDefinition = "bigint(10)", nullable = false)
    Long assessed = 0

    @Column(name = "assesstimestart", columnDefinition = "bigint(10)", nullable = false)
    Long assessTimeStart = 0

    @Column(name = "assesstimefinish", columnDefinition = "bigint(10)", nullable = false)
    Long assessTimeFinish = 0

    @Column(name = "scale", columnDefinition = "bigint(10)", nullable = false)
    Long scale = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "completionentries", columnDefinition = "int(9)", nullable = false)
    Integer completionEntries = 0


    @OneToMany(mappedBy = "glossary")
    Set<GlossaryCategory> categorySet

    @OneToMany(mappedBy = "glossary")
    Set<GlossaryEntry> entrySet

    @ManyToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}

