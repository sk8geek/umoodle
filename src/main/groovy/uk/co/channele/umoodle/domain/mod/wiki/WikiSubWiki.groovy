/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.wiki

import uk.co.channele.umoodle.domain.lib.group.Group
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="wiki_subwikis", uniqueConstraints = @UniqueConstraint(columnNames = ["wikiid", "groupid", "userid"]))
class WikiSubWiki {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id


    @OneToMany(mappedBy = "subWiki")
    Set<WikiLink> wikiLinkSet

    @OneToMany(mappedBy = "subWiki")
    Set<WikiSynonym> synonymSet

    @ManyToOne()
    @JoinColumn(name = "wikiid", columnDefinition = "bigint(10)", nullable = false)
    Wiki wiki

    @OneToOne()
    @JoinColumn(name = "groupid", columnDefinition = "bigint(10)", nullable = false)
    Group group

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
