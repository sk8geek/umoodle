package uk.co.channele.umoodle.domain.mod.assign.feedback.editpdf

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="assignfeedback_editpdf_rot", uniqueConstraints = @UniqueConstraint(columnNames = ["gradeid", "pageno"]))
class AssignFeedbackEditPdfRot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "gradeid", columnDefinition = "bigint(10)", nullable = false)
    Long gradeid = 0

    @Column(name = "pageno", columnDefinition = "bigint(10)", nullable = false)
    Long pageno = 0

    @Column(name = "pathnamehash", columnDefinition = "longtext", nullable = false)
    String pathnamehash

    @Column(name = "isrotated", columnDefinition = "tinyint(1)", nullable = false)
    Boolean isrotated = false

    @Column(name = "degree", columnDefinition = "bigint(10)", nullable = false)
    Long degree = 0


//        <KEY NAME="gradeid" TYPE="foreign" FIELDS="gradeid" REFTABLE="assign_grades" REFFIELDS="id"/>

}
