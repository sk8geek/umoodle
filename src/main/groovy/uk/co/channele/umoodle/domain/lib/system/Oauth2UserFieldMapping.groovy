/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "oauth2_user_field_mapping")
class Oauth2UserFieldMapping {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(nullable = false)
    Long timecreated

    @Column(nullable = false)
    Long usermodified

    @Column(nullable = false)
    Long issuerid

    @Column(length = 64, nullable = false)
    String externalfield = ""

    @Column(length = 64, nullable = false)
    String internalfield = ""


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="issuerkey" TYPE="foreign" FIELDS="issuerid" REFTABLE="oauth2_issuer" REFFIELDS="id"/>
// <KEY NAME="uniqinternal" TYPE="unique" FIELDS="issuerid, internalfield"/>
}