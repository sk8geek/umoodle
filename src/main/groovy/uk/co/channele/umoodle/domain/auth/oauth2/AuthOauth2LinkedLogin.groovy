/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.auth.oauth2

import uk.co.channele.umoodle.domain.lib.system.Oauth2Issuer
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="auth_oauth2_linked_login", uniqueConstraints = @UniqueConstraint(columnNames = ["userid", "issuerid", "username"]))
class AuthOauth2LinkedLogin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified

    @Column(name = "username", length = 255, nullable = false)
    String username

    @Column(name = "email", columnDefinition = "longtext", nullable = false)
    String email

    @Column(name = "confirmtoken", length = 64, nullable = false)
    String confirmToken

    @Column(name = "confirmtokenexpires", columnDefinition = "bigint(10)")
    Long confirmTokenExpires


    @OneToOne()
    @JoinColumn(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    User userModified

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "issuerid", columnDefinition = "bigint(10)", nullable = false)
    Oauth2Issuer issuer

}
