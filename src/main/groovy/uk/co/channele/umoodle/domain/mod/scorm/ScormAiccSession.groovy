/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.scorm

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="scorm_aicc_session")
class ScormAiccSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "hacpsession", length = 255, nullable = false)
    String hacpsession

    @Column(name = "scoid", columnDefinition = "bigint(10)")
    Long scoid = 0

    @Column(name = "scormmode", length = 50)
    String scormmode

    @Column(name = "scormstatus", length = 255)
    String scormstatus

    @Column(name = "attempt", columnDefinition = "bigint(10)")
    Long attempt

    @Column(name = "lessonstatus", length = 255)
    String lessonstatus

    @Column(name = "sessiontime", length = 255)
    String sessiontime

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0


    @OneToOne()
    @JoinColumn(name = "scormid", columnDefinition = "bigint(10)", nullable = false)
    Scorm scorm

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}