/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assign.feedback.editpdf

import uk.co.channele.umoodle.domain.mod.assign.AssignSubmission

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="assignfeedback_editpdf_queue", uniqueConstraints = @UniqueConstraint(columnNames = ["submissionid", "submissionattempt"]))
class AssignFeedbackEditPdfQueue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "submissionattempt", columnDefinition = "bigint(10)", nullable = false)
    Long submissionattempt

    @Column(name = "attemptedconversions", columnDefinition = "bigint(10)", nullable = false)
    Long attemptedconversions = 0


    @OneToOne()
    @JoinColumn(name = "submissionid", columnDefinition = "bigint(10)", nullable = false)
    AssignSubmission submission

}
