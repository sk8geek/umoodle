/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.block

import uk.co.channele.umoodle.domain.lib.context.Context
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "block_instances")
class BlockInstances {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 40, nullable = false)
    String blockname = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer showinsubcontexts

    @Column(columnDefinition = "smallint", nullable = false)
    Integer requiredbytheme = 0

    @Column(length = 64, nullable = false)
    String pagetypepattern = ""

    @Column(length = 16)
    String subpagepattern = ""

    @Column(length = 16, nullable = false)
    String defaultregion = ""

    @Column(nullable = false)
    Long defaultweight

    @Column(columnDefinition = "longtext")
    String configdata = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "blockInstance")
    Set<BlockPositions> blockPositionsSet

    @ManyToOne
    @JoinColumn(name = "parentcontextid", nullable = false)
    Context parentContext

}
