/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.badge

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "badge_issued")
class BadgeIssued {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "longtext", nullable = false)
    String uniquehash = ""

    @Column(name = "dateissued", nullable = false)
    Long dateIssued = 0

    @Column(name = "dateexpire")
    Long dateExpire

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visible = 0

    @Column(name = "issuernotified")
    Long issuerNotified


    @ManyToOne
    @JoinColumn(name = "badgeid", nullable = false)
    Badge badge

    @ManyToOne
    @JoinColumn(name = "userid", nullable = false)
    User user

}
