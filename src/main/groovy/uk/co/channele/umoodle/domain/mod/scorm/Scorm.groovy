/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.scorm

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="scorm")
class Scorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "scormtype", length = 50, nullable = false)
    String scormType

    @Column(name = "reference", length = 255, nullable = false)
    String reference

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introFormat = 0

    @Column(name = "version", length = 9, nullable = false)
    String version

    @Column(name = "maxgrade", columnDefinition = "double", nullable = false)
    BigDecimal maxGrade = BigDecimal.ZERO

    @Column(name = "grademethod", columnDefinition = "tinyint(2)", nullable = false)
    Integer gradeMethod = 0

    @Column(name = "whatgrade", columnDefinition = "bigint(10)", nullable = false)
    Long whatGrade = 0

    @Column(name = "maxattempt", columnDefinition = "bigint(10)", nullable = false)
    Long maxAttempt = 1

    @Column(name = "forcecompleted", columnDefinition = "bit(1)", nullable = false)
    Boolean forceCompleted = false

    @Column(name = "forcenewattempt", columnDefinition = "bit(1)", nullable = false)
    Boolean forceNewAttempt = false

    @Column(name = "lastattemptlock", columnDefinition = "bit(1)", nullable = false)
    Boolean lastAttemptLock = false

    @Column(name = "masteryoverride", columnDefinition = "bit(1)", nullable = false)
    Boolean masteryOverride = true

    @Column(name = "displayattemptstatus", columnDefinition = "bit(1)", nullable = false)
    Boolean displayAttemptStatus = true

    @Column(name = "displaycoursestructure", columnDefinition = "bit(1)", nullable = false)
    Boolean displayCourseStructure = false

    @Column(name = "updatefreq", columnDefinition = "bit(1)", nullable = false)
    Boolean updateFreq = false

    @Column(name = "sha1hash", length = 40)
    String sha1Hash

    @Column(name = "md5hash", length = 32, nullable = false)
    String md5Hash

    @Column(name = "revision", columnDefinition = "bigint(10)", nullable = false)
    Long revision = 0

    @Column(name = "launch", columnDefinition = "bigint(10)", nullable = false)
    Long launch = 0

    @Column(name = "skipview", columnDefinition = "bit(1)", nullable = false)
    Boolean skipView = true

    @Column(name = "hidebrowse", columnDefinition = "bit(1)", nullable = false)
    Boolean hideBrowse = false

    @Column(name = "hidetoc", columnDefinition = "bit(1)", nullable = false)
    Boolean hideToc = false

    @Column(name = "nav", columnDefinition = "bit(1)", nullable = false)
    Boolean nav = true

    @Column(name = "navpositionleft", columnDefinition = "bigint(10)")
    Long navPositionLeft = --100

    @Column(name = "navpositiontop", columnDefinition = "bigint(10)")
    Long navPositionTop = --100

    @Column(name = "auto", columnDefinition = "bit(1)", nullable = false)
    Boolean auto = false

    @Column(name = "popup", columnDefinition = "bit(1)", nullable = false)
    Boolean popup = false

    @Column(name = "options", length = 255, nullable = false)
    String options

    @Column(name = "width", columnDefinition = "bigint(10)", nullable = false)
    Long width = 100

    @Column(name = "height", columnDefinition = "bigint(10)", nullable = false)
    Long height = 600

    @Column(name = "timeopen", columnDefinition = "bigint(10)", nullable = false)
    Long timeOpen = 0

    @Column(name = "timeclose", columnDefinition = "bigint(10)", nullable = false)
    Long timeClose = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "completionstatusrequired", columnDefinition = "bit(1)")
    Boolean completionStatusRequired

    @Column(name = "completionscorerequired", columnDefinition = "bigint(10)")
    Long completionScoreRequired

    @Column(name = "completionstatusallscos", columnDefinition = "bit(1)")
    Boolean completionStatusAllScos

    @Column(name = "displayactivityname", columnDefinition = "smallint(4)", nullable = false)
    Integer displayActivityName = 1

    @Column(name = "autocommit", columnDefinition = "bit(1)", nullable = false)
    Boolean autoCommit = false


    @OneToMany(mappedBy = "scorm")
    Set<ScormScoes> scoesSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
