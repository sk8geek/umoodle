/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.message

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "message_read")
class MessageRead {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long useridfrom = 0

    @Column(nullable = false)
    Long useridto = 0

    @Column(columnDefinition = "longtext")
    String subject = ""

    @Column(columnDefinition = "longtext")
    String fullmessage = ""

    @Column(columnDefinition = "smallint")
    Integer fullmessageformat = 0

    @Column(columnDefinition = "longtext")
    String fullmessagehtml = ""

    @Column(columnDefinition = "longtext")
    String smallmessage = ""

    @Column(columnDefinition = "bit(1)")
    Integer notification

    @Column(columnDefinition = "longtext")
    String contexturl = ""

    @Column(columnDefinition = "longtext")
    String contexturlname = ""

    @Column(nullable = false)
    Long timecreated = 0

    @Column(nullable = false)
    Long timeread = 0

    @Column(nullable = false)
    Long timeuserfromdeleted = 0

    @Column(nullable = false)
    Long timeusertodeleted = 0

    @Column(length = 100)
    String component = ""

    @Column(length = 100)
    String eventtype = ""


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="useridfromtodeleted" UNIQUE="false" FIELDS="useridfrom, useridto, timeuserfromdeleted, timeusertodeleted"/>
// <INDEX NAME="notificationtimeread" UNIQUE="false" FIELDS="notification, timeread"/>
// <INDEX NAME="useridfrom_timeuserfromdeleted_notification" UNIQUE="false" FIELDS="useridfrom, timeuserfromdeleted, notification"/>
// <INDEX NAME="useridto_timeusertodeleted_notification" UNIQUE="false" FIELDS="useridto, timeusertodeleted, notification"/>
}
