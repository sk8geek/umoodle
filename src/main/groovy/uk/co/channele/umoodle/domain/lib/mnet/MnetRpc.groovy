/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.mnet

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "mnet_rpc")
class MnetRpc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 40, nullable = false)
    String functionname = ""

    @Column(length = 80, nullable = false)
    String xmlrpcpath = ""

    @Column(length = 20, nullable = false)
    String plugintype = ""

    @Column(length = 20, nullable = false)
    String pluginname = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer enabled = 0

    @Column(columnDefinition = "longtext", nullable = false)
    String help = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String profile = ""

    @Column(length = 100, nullable = false)
    String filename = ""

    @Column(length = 150)
    String classname = ""

    @Column(name = "static", columnDefinition = "bit(1)")
    Integer isStatic


// <KEY NAME="primary" TYPE="primary" FIELDS="id" COMMENT="primary key of the mnet_rpc table"/>

// <INDEX NAME="enabled_xmlrpcpath" UNIQUE="false" FIELDS="enabled, xmlrpcpath"/>
}
