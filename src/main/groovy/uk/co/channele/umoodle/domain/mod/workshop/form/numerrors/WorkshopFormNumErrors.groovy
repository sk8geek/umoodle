/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.workshop.form.numerrors

import uk.co.channele.umoodle.domain.mod.workshop.Workshop

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="workshopform_numerrors")
class WorkshopFormNumErrors {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "sort", columnDefinition = "bigint(10)")
    Long sort = 0

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "smallint(3)")
    Integer descriptionformat = 0

    @Column(name = "descriptiontrust", columnDefinition = "bigint(10)")
    Long descriptiontrust

    @Column(name = "grade0", length = 50)
    String grade0

    @Column(name = "grade1", length = 50)
    String grade1

    @Column(name = "weight", columnDefinition = "mediumint(5)")
    Integer weight = 1


    @OneToOne()
    @JoinColumn(name = "workshopid", columnDefinition = "bigint(10)", nullable = false)
    Workshop workshop

}
