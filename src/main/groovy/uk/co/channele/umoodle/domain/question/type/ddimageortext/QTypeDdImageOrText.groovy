/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.ddimageortext

import uk.co.channele.umoodle.domain.lib.question.Question
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="qtype_ddimageortext")
class QTypeDdImageOrText {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "shuffleanswers", columnDefinition = "smallint(4)", nullable = false)
    Integer shuffleAnswers = 1

    @Column(name = "correctfeedback", columnDefinition = "longtext", nullable = false)
    String correctFeedback

    @Column(name = "correctfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer correctFeedbackFormat = 0

    @Column(name = "partiallycorrectfeedback", columnDefinition = "longtext", nullable = false)
    String partiallyCorrectFeedback

    @Column(name = "partiallycorrectfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer partiallyCorrectFeedbackFormat = 0

    @Column(name = "incorrectfeedback", columnDefinition = "longtext", nullable = false)
    String incorrectFeedback

    @Column(name = "incorrectfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer incorrectFeedbackFormat = 0

    @Column(name = "shownumcorrect", columnDefinition = "tinyint(2)", nullable = false)
    Integer showNumCorrect = 0


    @ManyToOne()
    @JoinColumn(name = "questionid", columnDefinition = "bigint(10)", nullable = false)
    Question question

}
