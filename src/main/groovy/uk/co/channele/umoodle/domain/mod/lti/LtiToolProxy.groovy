/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lti

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name ="lti_tool_proxies")
class LtiToolProxy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "regurl", columnDefinition = "longtext")
    String regUrl

    @Column(name = "state", columnDefinition = "tinyint(2)", nullable = false)
    Integer state = 1

    @Column(name = "guid", length = 255, unique = true)
    String guid

    @Column(name = "secret", length = 255)
    String secret

    @Column(name = "vendorcode", length = 255)
    String vendorCode

    @Column(name = "capabilityoffered", columnDefinition = "longtext", nullable = false)
    String capabilityOffered

    @Column(name = "serviceoffered", columnDefinition = "longtext", nullable = false)
    String serviceOffered

    @Column(name = "toolproxy", columnDefinition = "longtext")
    String toolProxy

    @Column(name = "createdby", columnDefinition = "bigint(10)", nullable = false)
    Long createdBy

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "ltiToolProxy")
    Set<LtiToolSetting> ltiToolSettingSet

}