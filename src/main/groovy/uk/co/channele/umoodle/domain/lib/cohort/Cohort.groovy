/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.cohort

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "cohort")
class Cohort {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 254, nullable = false)
    String name = ""

    @Column(name = "idnumber", length = 100)
    String idNumber = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(name = "descriptionformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer descriptionFormat

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visible = 1

    @Column(length = 100, nullable = false)
    String component = ""

    @Column(length = 50)
    String theme = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @Column(nullable = false)
    Long contextid

// <KEY NAME="context" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>
}
