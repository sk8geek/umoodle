/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.role

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "role_assignments")
class RoleAssignments {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long roleid = 0

    @Column(nullable = false)
    Long contextid = 0

    @Column(nullable = false)
    Long userid = 0

    @Column(nullable = false)
    Long timemodified = 0

    @Column(nullable = false)
    Long modifierid = 0

    @Column(length = 100, nullable = false)
    String component = ""

    @Column(nullable = false)
    Long itemid = 0

    @Column(nullable = false)
    Long sortorder = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="roleid" TYPE="foreign" FIELDS="roleid" REFTABLE="role" REFFIELDS="id"/>
// <KEY NAME="contextid" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>
// <KEY NAME="userid" TYPE="foreign" FIELDS="userid" REFTABLE="user" REFFIELDS="id"/>

// <INDEX NAME="sortorder" UNIQUE="false" FIELDS="sortorder"/>
// <INDEX NAME="rolecontext" UNIQUE="false" FIELDS="roleid, contextid" COMMENT="Index on roleid and contextid"/>
// <INDEX NAME="usercontextrole" UNIQUE="false" FIELDS="userid, contextid, roleid" COMMENT="Index on userid, contextid and roleid"/>
// <INDEX NAME="component-itemid-userid" UNIQUE="false" FIELDS="component, itemid, userid"/>
}
