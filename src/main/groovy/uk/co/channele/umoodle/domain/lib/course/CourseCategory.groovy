/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "course_categories")
@JsonIgnoreProperties(["courseSet"])
class CourseCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(name = "idnumber", length = 100)
    String idNumber = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(name = "descriptionformat", columnDefinition = "tinyint", nullable = false)
    Integer descriptionFormat = 0

    @Column(nullable = false)
    Long parent = 0

    @Column(name = "sortorder", nullable = false)
    Long sortOrder = 0

    @Column(name = "coursecount", nullable = false)
    Long courseCount = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Boolean visible = true

    /**
     * The state of the the <code>visible</code> field when hiding the parent category.
     */
    @Column(name = "visibleold", columnDefinition = "bit(1)", nullable = false)
    Boolean visibleOld = true

    @Column(name = "timemodified", nullable = false)
    Long timeModified = 0

    @Column(nullable = false)
    Long depth = 0

    @Column(length = 255, nullable = false)
    String path = ""

    @Column(length = 50)
    String theme = ""


    @OneToMany(mappedBy = "category")
    Set<Course> courseSet

// <KEY NAME="parent" TYPE="foreign" FIELDS="parent" REFTABLE="course_categories" REFFIELDS="id" COMMENT="note that to make this recursive FK working someday, the parent field must be declared NULL"/>
}
