/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.grade.grading.form.rubric

import uk.co.channele.umoodle.domain.lib.context.GradingInstance
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="gradingform_rubric_fillings", uniqueConstraints = @UniqueConstraint(columnNames = ["instanceid", "criterionid"]))
class GradingFormRubricFilling {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    /** levelid acts as a foreign key but null values are allowed */
    @Column(name = "levelid", columnDefinition = "bigint(10)")
    Long levelId

    @Column(name = "remark", columnDefinition = "longtext")
    String remark

    @Column(name = "remarkformat", columnDefinition = "tinyint(2)")
    Integer remarkFormat


    @ManyToOne()
    @JoinColumn(name = "instanceid", columnDefinition = "bigint(10)", nullable = false)
    GradingInstance gradingInstance

    @ManyToOne()
    @JoinColumn(name = "criterionid", columnDefinition = "bigint(10)", nullable = false)
    GradingFormRubricCriteria rubricCriteria

}
