/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lesson

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="lesson")
class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext")
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "practice", columnDefinition = "smallint(3)", nullable = false)
    Integer practice = 0

    @Column(name = "modattempts", columnDefinition = "smallint(3)", nullable = false)
    Integer modattempts = 0

    @Column(name = "usepassword", columnDefinition = "smallint(3)", nullable = false)
    Integer usepassword = 0

    @Column(name = "password", length = 32, nullable = false)
    String password

    @Column(name = "dependency", columnDefinition = "bigint(10)", nullable = false)
    Long dependency = 0

    @Column(name = "conditions", columnDefinition = "longtext", nullable = false)
    String conditions

    @Column(name = "grade", columnDefinition = "bigint(10)", nullable = false)
    Long grade = 0

    @Column(name = "custom", columnDefinition = "smallint(3)", nullable = false)
    Integer custom = 0

    @Column(name = "ongoing", columnDefinition = "smallint(3)", nullable = false)
    Integer ongoing = 0

    @Column(name = "usemaxgrade", columnDefinition = "smallint(3)", nullable = false)
    Integer usemaxgrade = 0

    @Column(name = "maxanswers", columnDefinition = "smallint(3)", nullable = false)
    Integer maxanswers = 4

    @Column(name = "maxattempts", columnDefinition = "smallint(3)", nullable = false)
    Integer maxattempts = 5

    @Column(name = "review", columnDefinition = "smallint(3)", nullable = false)
    Integer review = 0

    @Column(name = "nextpagedefault", columnDefinition = "smallint(3)", nullable = false)
    Integer nextpagedefault = 0

    @Column(name = "feedback", columnDefinition = "smallint(3)", nullable = false)
    Integer feedback = 1

    @Column(name = "minquestions", columnDefinition = "smallint(3)", nullable = false)
    Integer minquestions = 0

    @Column(name = "maxpages", columnDefinition = "smallint(3)", nullable = false)
    Integer maxpages = 0

    @Column(name = "timelimit", columnDefinition = "bigint(10)", nullable = false)
    Long timelimit = 0

    @Column(name = "retake", columnDefinition = "smallint(3)", nullable = false)
    Integer retake = 1

    @Column(name = "activitylink", columnDefinition = "bigint(10)", nullable = false)
    Long activitylink = 0

    @Column(name = "mediafile", length = 255, nullable = false)
    String mediafile

    @Column(name = "mediaheight", columnDefinition = "bigint(10)", nullable = false)
    Long mediaheight = 100

    @Column(name = "mediawidth", columnDefinition = "bigint(10)", nullable = false)
    Long mediawidth = 650

    @Column(name = "mediaclose", columnDefinition = "smallint(3)", nullable = false)
    Integer mediaclose = 0

    @Column(name = "slideshow", columnDefinition = "smallint(3)", nullable = false)
    Integer slideshow = 0

    @Column(name = "width", columnDefinition = "bigint(10)", nullable = false)
    Long width = 640

    @Column(name = "height", columnDefinition = "bigint(10)", nullable = false)
    Long height = 480

    @Column(name = "bgcolor", length = 7, nullable = false)
    String bgcolor

    @Column(name = "displayleft", columnDefinition = "smallint(3)", nullable = false)
    Integer displayleft = 0

    @Column(name = "displayleftif", columnDefinition = "smallint(3)", nullable = false)
    Integer displayleftif = 0

    @Column(name = "progressbar", columnDefinition = "smallint(3)", nullable = false)
    Integer progressbar = 0

    @Column(name = "available", columnDefinition = "bigint(10)", nullable = false)
    Long available = 0

    @Column(name = "deadline", columnDefinition = "bigint(10)", nullable = false)
    Long deadline = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "completionendreached", columnDefinition = "bit(1)")
    Boolean completionendreached = false

    @Column(name = "completiontimespent", columnDefinition = "bigint(11)")
    Long completiontimespent = 0

    @Column(name = "allowofflineattempts", columnDefinition = "bit(1)")
    Boolean allowofflineattempts = false


    @OneToMany(mappedBy = "lesson")
    Set<LessonAnswer> answerSet

    @OneToMany(mappedBy = "lesson")
    Set<LessonPage> pageSet

    @ManyToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
