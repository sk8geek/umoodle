/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.message

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "notifications")
class Notification {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "longtext")
    String subject = ""

    @Column(name = "fullmessage", columnDefinition = "longtext")
    String fullMessage = ""

    @Column(name = "fullmessageformat", columnDefinition = "bit(1)", nullable = false)
    Integer fullMessageFormat = 0

    @Column(name = "fullmessagehtml", columnDefinition = "longtext")
    String fullMessageHtml = ""

    @Column(name = "smallmessage", columnDefinition = "longtext")
    String smallMessage = ""

    @Column(length = 100)
    String component = ""

    @Column(name = "eventtype", length = 100)
    String eventType = ""

    @Column(name = "contexturl", columnDefinition = "longtext")
    String contextUrl = ""

    @Column(name = "contexturlname", columnDefinition = "longtext")
    String contextUrlName = ""

    @Column(name = "timeread")
    Long timeRead

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @ManyToOne()
    @JoinColumn(name = "useridfrom", nullable = false)
    User fromUser

    @ManyToOne()
    @JoinColumn(name = "useridto", nullable = false)
    User toUser



// <KEY NAME="useridto" TYPE="foreign" FIELDS="useridto" REFTABLE="user" REFFIELDS="id"/>

}
