/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.enrol

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.course.Enrol
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="enrol_paypal")
class EnrolPaypal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "business", length = 255, nullable = false)
    String business

    @Column(name = "receiver_email", length = 255, nullable = false)
    String receiver_email

    @Column(name = "receiver_id", length = 255, nullable = false)
    String receiver_id

    @Column(name = "item_name", length = 255, nullable = false)
    String item_name

    @Column(name = "memo", length = 255, nullable = false)
    String memo

    @Column(name = "tax", length = 255, nullable = false)
    String tax

    @Column(name = "option_name1", length = 255, nullable = false)
    String option_name1

    @Column(name = "option_selection1_x", length = 255, nullable = false)
    String option_selection1_x

    @Column(name = "option_name2", length = 255, nullable = false)
    String option_name2

    @Column(name = "option_selection2_x", length = 255, nullable = false)
    String option_selection2_x

    @Column(name = "payment_status", length = 255, nullable = false)
    String payment_status

    @Column(name = "pending_reason", length = 255, nullable = false)
    String pending_reason

    @Column(name = "reason_code", length = 30, nullable = false)
    String reason_code

    @Column(name = "txn_id", length = 255, nullable = false)
    String txn_id

    @Column(name = "parent_txn_id", length = 255, nullable = false)
    String parent_txn_id

    @Column(name = "payment_type", length = 30, nullable = false)
    String payment_type

    @Column(name = "timeupdated", columnDefinition = "bigint(10)", nullable = false)
    Long timeupdated = 0


    @OneToOne()
    @JoinColumn(name = "courseid", columnDefinition = "bigint(10)", nullable = false)
    Course course

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "instanceid", columnDefinition = "bigint(10)", nullable = false)
    Enrol enrolInstance

}
