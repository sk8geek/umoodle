/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.workshop

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="workshopallocation_scheduled")
class WorkshopAllocationScheduled {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "enabled", columnDefinition = "tinyint(2)", nullable = false)
    Integer enabled = 0

    @Column(name = "submissionend", columnDefinition = "bigint(10)", nullable = false)
    Long submissionEnd

    @Column(name = "timeallocated", columnDefinition = "bigint(10)")
    Long timeAllocated

    @Column(name = "settings", columnDefinition = "longtext")
    String settings

    @Column(name = "resultstatus", columnDefinition = "bigint(10)")
    Long resultStatus

    @Column(name = "resultmessage", length = 1333)
    String resultMessage

    @Column(name = "resultlog", columnDefinition = "longtext")
    String resultLog


    @ManyToOne()
    @JoinColumn(name = "workshopid", columnDefinition = "bigint(10)", nullable = false)
    Workshop workshop

}
