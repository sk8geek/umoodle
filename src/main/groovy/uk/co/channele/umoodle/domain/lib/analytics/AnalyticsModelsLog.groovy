/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.analytics

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "analytics_models_log")
class AnalyticsModelsLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long version

    @Column(length = 50, nullable = false)
    String evaluationmode = ""

    @Column(length = 255, nullable = false)
    String target = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String indicators = ""

    @Column(length = 255)
    String timesplitting = ""

    @Column(columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal score

    @Column(columnDefinition = "longtext")
    String info = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String dir = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @ManyToOne()
    @JoinColumn(name = "modelid", nullable = false)
    AnalyticsModel analyticsModel

    @OneToOne
    @JoinColumn(name = "usermodified", nullable = false)
    User userModified

}
