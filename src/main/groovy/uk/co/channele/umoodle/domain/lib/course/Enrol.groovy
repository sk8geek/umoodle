/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import uk.co.channele.umoodle.domain.lib.user.UserEnrolment
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "enrol")
class Enrol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 20, nullable = false)
    String enrol = ""

    @Column(nullable = false)
    Long status = 0

    @Column(nullable = false)
    Long sortorder = 0

    @Column(length = 255)
    String name = ""

    @Column
    Long enrolperiod = 0

    @Column
    Long enrolstartdate = 0

    @Column
    Long enrolenddate = 0

    @Column(columnDefinition = "bit(1)")
    Integer expirynotify

    @Column
    Long expirythreshold = 0

    @Column(columnDefinition = "bit(1)")
    Integer notifyall

    @Column(length = 50)
    String password = ""

    @Column(length = 20)
    String cost = ""

    @Column(length = 3)
    String currency = ""

    @Column
    Long roleid = 0

    @Column
    Long customint1

    @Column
    Long customint2

    @Column
    Long customint3

    @Column
    Long customint4

    @Column
    Long customint5

    @Column
    Long customint6

    @Column
    Long customint7

    @Column
    Long customint8

    @Column(length = 255)
    String customchar1 = ""

    @Column(length = 255)
    String customchar2 = ""

    @Column(length = 1333)
    String customchar3 = ""

    @Column(columnDefinition = "decimal(12,7)")
    BigDecimal customdec1

    @Column(columnDefinition = "decimal(12,7)")
    BigDecimal customdec2

    @Column(columnDefinition = "longtext")
    String customtext1 = ""

    @Column(columnDefinition = "longtext")
    String customtext2 = ""

    @Column(columnDefinition = "longtext")
    String customtext3 = ""

    @Column(columnDefinition = "longtext")
    String customtext4 = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "enrol")
    Set<UserEnrolment> userEnrolmentSet

    @ManyToOne
    @JoinColumn(name = "courseid", nullable = false)
    Course course

}
