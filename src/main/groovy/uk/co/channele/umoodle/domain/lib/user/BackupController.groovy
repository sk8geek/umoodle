/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.user

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "backup_controllers")
class BackupController {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 32, nullable = false, unique = true)
    String backupid = ""

    @Column(length = 20, nullable = false)
    String operation = "backup"

    @Column(length = 10, nullable = false)
    String type = ""

    @Column(nullable = false)
    Long itemid

    @Column(length = 20, nullable = false)
    String format = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer interactive

    @Column(columnDefinition = "smallint", nullable = false)
    Integer purpose

    @Column(columnDefinition = "smallint", nullable = false)
    Integer status

    @Column(columnDefinition = "smallint", nullable = false)
    Integer execution

    @Column(nullable = false)
    Long executiontime

    @Column(length = 32, nullable = false)
    String checksum = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(columnDefinition = "longtext", nullable = false)
    String controller = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @ManyToOne
    @JoinColumn(name = "userid", nullable = false)
    User user

    @OneToMany(mappedBy = "backupController")
    Set<BackupLog> backupLogSet

}
