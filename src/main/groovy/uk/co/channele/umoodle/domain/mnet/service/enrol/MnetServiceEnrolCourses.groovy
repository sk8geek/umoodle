/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mnet.service.enrol

import uk.co.channele.umoodle.domain.lib.role.Role
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="mnetservice_enrol_courses", uniqueConstraints = @UniqueConstraint(columnNames = ["hostid", "remoteid"]))
class MnetServiceEnrolCourses {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "hostid", columnDefinition = "bigint(10)", nullable = false)
    Long hostId

    @Column(name = "remoteid", columnDefinition = "bigint(10)", nullable = false)
    Long remoteId

    @Column(name = "categoryid", columnDefinition = "bigint(10)", nullable = false)
    Long categoryId

    @Column(name = "categoryname", length = 255, nullable = false)
    String categoryName

    @Column(name = "sortorder", columnDefinition = "bigint(10)", nullable = false)
    Long sortOrder = 0

    @Column(name = "fullname", length = 254, nullable = false)
    String fullName

    @Column(name = "shortname", length = 100, nullable = false)
    String shortName

    @Column(name = "idnumber", length = 100, nullable = false)
    String idNumber

    @Column(name = "summary", columnDefinition = "longtext", nullable = false)
    String summary

    @Column(name = "summaryformat", columnDefinition = "smallint(3)")
    Integer summaryFormat = 0

    @Column(name = "startdate", columnDefinition = "bigint(10)", nullable = false)
    Long startDate

    @Column(name = "rolename", length = 255, nullable = false)
    String roleName


    @OneToOne()
    @JoinColumn(name = "roleid", columnDefinition = "bigint(10)", nullable = false)
    Role role

}