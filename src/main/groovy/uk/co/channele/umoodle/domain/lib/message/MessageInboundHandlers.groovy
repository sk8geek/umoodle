/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.message

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "messageinbound_handlers")
class MessageInboundHandlers {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 100, nullable = false)
    String component = ""

    @Column(length = 255, nullable = false)
    String classname = ""

    @Column(nullable = false)
    Long defaultexpiration = 86400

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer validateaddress = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer enabled = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="classname" TYPE="unique" FIELDS="classname" COMMENT="The classname must be unique."/>
}
