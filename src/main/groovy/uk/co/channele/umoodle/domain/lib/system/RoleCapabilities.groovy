/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "role_capabilities")
class RoleCapabilities {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long contextid = 0

    @Column(nullable = false)
    Long roleid = 0

    @Column(length = 255, nullable = false)
    String capability = ""

    @Column(nullable = false)
    Long permission = 0

    @Column(nullable = false)
    Long timemodified = 0

    @Column(nullable = false)
    Long modifierid = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="roleid" TYPE="foreign" FIELDS="roleid" REFTABLE="role" REFFIELDS="id"/>
// <KEY NAME="contextid" TYPE="foreign" FIELDS="contextid" REFTABLE="context" REFFIELDS="id"/>
// <KEY NAME="modifierid" TYPE="foreign" FIELDS="modifierid" REFTABLE="user" REFFIELDS="id"/>
// <KEY NAME="capability" TYPE="foreign" FIELDS="capability" REFTABLE="capabilities" REFFIELDS="name"/>

// <INDEX NAME="roleid-contextid-capability" UNIQUE="true" FIELDS="roleid, contextid, capability"/>
}
