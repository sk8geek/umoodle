/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assignment

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assignment_submissions")
class AssignmentSubmissions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "numfiles", columnDefinition = "bigint(10)", nullable = false)
    Long numFiles = 0

    @Column(name = "data1", columnDefinition = "longtext")
    String data1

    @Column(name = "data2", columnDefinition = "longtext")
    String data2

    @Column(name = "grade", columnDefinition = "bigint(11)", nullable = false)
    Long grade = 0

    @Column(name = "submissioncomment", columnDefinition = "longtext", nullable = false)
    String submissionComment

    @Column(name = "format", columnDefinition = "smallint(4)", nullable = false)
    Integer format = 0

    @Column(name = "teacher", columnDefinition = "bigint(10)", nullable = false)
    Long teacher = 0

    @Column(name = "timemarked", columnDefinition = "bigint(10)", nullable = false)
    Long timeMarked = 0

    @Column(name = "mailed", columnDefinition = "bit(1)", nullable = false)
    Boolean mailed = false


    @ManyToOne()
    @JoinColumn(name = "assignment", columnDefinition = "bigint(10)", nullable = false)
    Assignment assignment

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
