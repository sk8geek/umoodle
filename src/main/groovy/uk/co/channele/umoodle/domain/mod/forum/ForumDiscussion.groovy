/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.forum

import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="forum_discussions")
class ForumDiscussion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "firstpost", columnDefinition = "bigint(10)", nullable = false)
    Long firstpost = 0

    @Column(name = "groupid", columnDefinition = "bigint(10)", nullable = false)
    Long groupid = -1

    @Column(name = "assessed", columnDefinition = "bit(1)", nullable = false)
    Boolean assessed = true

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    Long usermodified = 0

    @Column(name = "timestart", columnDefinition = "bigint(10)", nullable = false)
    Long timestart = 0

    @Column(name = "timeend", columnDefinition = "bigint(10)", nullable = false)
    Long timeend = 0

    @Column(name = "pinned", columnDefinition = "bit(1)", nullable = false)
    Boolean pinned = false


    @ManyToOne()
    @JoinColumn(name = "forum", columnDefinition = "bigint(10)", nullable = false)
    Forum forum

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
