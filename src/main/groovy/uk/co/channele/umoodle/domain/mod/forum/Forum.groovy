/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.forum

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="forum")
class Forum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "type", length = 20, nullable = false)
    String type

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "assessed", columnDefinition = "bigint(10)", nullable = false)
    Long assessed = 0

    @Column(name = "assesstimestart", columnDefinition = "bigint(10)", nullable = false)
    Long assesstimestart = 0

    @Column(name = "assesstimefinish", columnDefinition = "bigint(10)", nullable = false)
    Long assesstimefinish = 0

    @Column(name = "scale", columnDefinition = "bigint(10)", nullable = false)
    Long scale = 0

    @Column(name = "maxbytes", columnDefinition = "bigint(10)", nullable = false)
    Long maxbytes = 0

    @Column(name = "maxattachments", columnDefinition = "bigint(10)", nullable = false)
    Long maxattachments = 1

    @Column(name = "forcesubscribe", columnDefinition = "tinyint(1)", nullable = false)
    Boolean forcesubscribe = false

    @Column(name = "trackingtype", columnDefinition = "tinyint(2)", nullable = false)
    Integer trackingtype = 1

    @Column(name = "rsstype", columnDefinition = "tinyint(2)", nullable = false)
    Integer rsstype = 0

    @Column(name = "rssarticles", columnDefinition = "tinyint(2)", nullable = false)
    Integer rssarticles = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "warnafter", columnDefinition = "bigint(10)", nullable = false)
    Long warnafter = 0

    @Column(name = "blockafter", columnDefinition = "bigint(10)", nullable = false)
    Long blockafter = 0

    @Column(name = "blockperiod", columnDefinition = "bigint(10)", nullable = false)
    Long blockperiod = 0

    @Column(name = "completiondiscussions", columnDefinition = "int(9)", nullable = false)
    Integer completiondiscussions = 0

    @Column(name = "completionreplies", columnDefinition = "int(9)", nullable = false)
    Integer completionreplies = 0

    @Column(name = "completionposts", columnDefinition = "int(9)", nullable = false)
    Integer completionposts = 0

    @Column(name = "displaywordcount", columnDefinition = "tinyint(1)", nullable = false)
    Boolean displaywordcount = false

    @Column(name = "lockdiscussionafter", columnDefinition = "bigint(10)", nullable = false)
    Long lockdiscussionafter = 0


    @OneToMany(mappedBy = "parent")
    Set<ForumPost> postSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
