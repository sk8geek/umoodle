/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.workshop

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="workshop")
class Workshop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext")
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(3)", nullable = false)
    Integer introformat = 0

    @Column(name = "instructauthors", columnDefinition = "longtext")
    String instructauthors

    @Column(name = "instructauthorsformat", columnDefinition = "smallint(3)", nullable = false)
    Integer instructauthorsformat = 0

    @Column(name = "instructreviewers", columnDefinition = "longtext")
    String instructreviewers

    @Column(name = "instructreviewersformat", columnDefinition = "smallint(3)", nullable = false)
    Integer instructreviewersformat = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "phase", columnDefinition = "smallint(3)")
    Integer phase = 0

    @Column(name = "useexamples", columnDefinition = "tinyint(2)")
    Integer useexamples = 0

    @Column(name = "usepeerassessment", columnDefinition = "tinyint(2)")
    Integer usepeerassessment = 0

    @Column(name = "useselfassessment", columnDefinition = "tinyint(2)")
    Integer useselfassessment = 0

    @Column(name = "grade", columnDefinition = "decimal(10,5)")
    BigDecimal grade=80

    @Column(name = "gradinggrade", columnDefinition = "decimal(10,5)")
    BigDecimal gradinggrade=20

    @Column(name = "strategy", length = 30, nullable = false)
    String strategy

    @Column(name = "evaluation", length = 30, nullable = false)
    String evaluation

    @Column(name = "gradedecimals", columnDefinition = "smallint(3)")
    Integer gradedecimals = 0

    @Column(name = "submissiontypetext", columnDefinition = "bit(1)", nullable = false)
    Boolean submissiontypetext = true

    @Column(name = "submissiontypefile", columnDefinition = "bit(1)", nullable = false)
    Boolean submissiontypefile = true

    @Column(name = "nattachments", columnDefinition = "smallint(3)")
    Integer nattachments = 1

    @Column(name = "submissionfiletypes", length = 255)
    String submissionfiletypes

    @Column(name = "latesubmissions", columnDefinition = "tinyint(2)")
    Integer latesubmissions = 0

    @Column(name = "maxbytes", columnDefinition = "bigint(10)")
    Long maxbytes = 100000

    @Column(name = "examplesmode", columnDefinition = "smallint(3)")
    Integer examplesmode = 0

    @Column(name = "submissionstart", columnDefinition = "bigint(10)")
    Long submissionstart = 0

    @Column(name = "submissionend", columnDefinition = "bigint(10)")
    Long submissionend = 0

    @Column(name = "assessmentstart", columnDefinition = "bigint(10)")
    Long assessmentstart = 0

    @Column(name = "assessmentend", columnDefinition = "bigint(10)")
    Long assessmentend = 0

    @Column(name = "phaseswitchassessment", columnDefinition = "tinyint(2)", nullable = false)
    Integer phaseswitchassessment = 0

    @Column(name = "conclusion", columnDefinition = "longtext")
    String conclusion

    @Column(name = "conclusionformat", columnDefinition = "smallint(3)", nullable = false)
    Integer conclusionformat = 1

    @Column(name = "overallfeedbackmode", columnDefinition = "smallint(3)")
    Integer overallfeedbackmode = 1

    @Column(name = "overallfeedbackfiles", columnDefinition = "smallint(3)")
    Integer overallfeedbackfiles = 0

    @Column(name = "overallfeedbackfiletypes", length = 255)
    String overallfeedbackfiletypes

    @Column(name = "overallfeedbackmaxbytes", columnDefinition = "bigint(10)")
    Long overallfeedbackmaxbytes = 100000


    @OneToMany(mappedBy = "workshop")
    Set<WorkshopAllocationScheduled> allocationScheduledSet

    @OneToMany(mappedBy = "workshop")
    Set<WorkshopSubmission> submissionSet


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
