/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.grade.grading.form.rubric

import uk.co.channele.umoodle.domain.lib.context.GradingDefinition
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name ="gradingform_rubric_criteria")
class GradingFormRubricCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "sortorder", columnDefinition = "bigint(10)", nullable = false)
    Long sortOrder

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "tinyint(2)")
    Integer descriptionFormat


    @OneToMany(mappedBy = "rubricCriteria")
    Set<GradingFormRubricFilling> rubricFillingSet

    @OneToMany(mappedBy = "rubricCriteria")
    Set<GradingFormRubricLevel> rubricLevelSet

    @ManyToOne()
    @JoinColumn(name = "definitionid", columnDefinition = "bigint(10)", nullable = false)
    GradingDefinition definition

}
