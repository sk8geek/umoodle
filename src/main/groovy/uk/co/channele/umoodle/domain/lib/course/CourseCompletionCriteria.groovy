/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "course_completion_criteria")
class CourseCompletionCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long criteriatype = 0

    @Column(length = 100)
    String module = ""

    @Column
    Long moduleinstance

    @Column
    Long courseinstance

    @Column
    Long enrolperiod

    @Column
    Long timeend

    @Column(columnDefinition = "decimal(10,5)")
    BigDecimal gradepass

    @Column
    Long role

    @ManyToOne
    @JoinColumn(name = "course", nullable = false)
    Course course


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="course" UNIQUE="false" FIELDS="course"/>
}
