/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lesson

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="lesson_pages")
class LessonPage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "prevpageid", columnDefinition = "bigint(10)", nullable = false)
    Long prevpageid = 0

    @Column(name = "nextpageid", columnDefinition = "bigint(10)", nullable = false)
    Long nextpageid = 0

    @Column(name = "qtype", columnDefinition = "smallint(3)", nullable = false)
    Integer qtype = 0

    @Column(name = "qoption", columnDefinition = "smallint(3)", nullable = false)
    Integer qoption = 0

    @Column(name = "layout", columnDefinition = "smallint(3)", nullable = false)
    Integer layout = 1

    @Column(name = "display", columnDefinition = "smallint(3)", nullable = false)
    Integer display = 1

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "title", length = 255, nullable = false)
    String title

    @Column(name = "contents", columnDefinition = "longtext", nullable = false)
    String contents

    @Column(name = "contentsformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer contentsformat = 0


    @ManyToOne()
    @JoinColumn(name = "lessonid", columnDefinition = "bigint(10)", nullable = false)
    Lesson lesson

}
