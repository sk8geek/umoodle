/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.dataprivacy

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="tool_dataprivacy_request")
class ToolDataPrivacyRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "type", columnDefinition = "bigint(10)", nullable = false)
    Long type = 0

    @Column(name = "comments", columnDefinition = "longtext")
    String comments

    @Column(name = "commentsformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer commentsFormat = 0

    @Column(name = "status", columnDefinition = "tinyint(2)", nullable = false)
    Integer status = 0

    @Column(name = "dpocomment", columnDefinition = "longtext")
    String dpoComment

    @Column(name = "dpocommentformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer dpoCommentFormat = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "creationmethod", columnDefinition = "bigint(10)", nullable = false)
    Long creationMethod = 0


    @ManyToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "requestedby", columnDefinition = "bigint(10)", nullable = false)
    User requestedBy

    @OneToOne()
    @JoinColumn(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    User modifiedBy

    @OneToOne()
    @JoinColumn(name = "dpo", columnDefinition = "bigint(10)")
    User dpo

}
