/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.question

import uk.co.channele.umoodle.domain.mod.quiz.report.QuizOverviewRegrade

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "question_attempts", uniqueConstraints = @UniqueConstraint(columnNames = ["questionusageid", "slot"]))
class QuestionAttempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long questionusageid

    @Column(nullable = false)
    Long slot

    @Column(length = 32, nullable = false)
    String behaviour = ""

    @Column(nullable = false)
    Long variant = 1

    @Column(columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal maxmark

    @Column(columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal minfraction

    @Column(columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal maxfraction = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer flagged = 0

    @Column(columnDefinition = "longtext")
    String questionsummary = ""

    @Column(columnDefinition = "longtext")
    String rightanswer = ""

    @Column(columnDefinition = "longtext")
    String responsesummary = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "questionUsage")
    Set<QuizOverviewRegrade> quizOverviewRegradeSet

    @ManyToOne()
    @JoinColumn(name = "questionid", nullable = false)
    Question question


// <KEY NAME="questionusageid" TYPE="foreign" FIELDS="questionusageid" REFTABLE="question_usages" REFFIELDS="id"/>

// <INDEX NAME="behaviour" UNIQUE="false" FIELDS="behaviour" COMMENT="Required because we need to be able to determine efficiently which behaviours are in use."/>
}
