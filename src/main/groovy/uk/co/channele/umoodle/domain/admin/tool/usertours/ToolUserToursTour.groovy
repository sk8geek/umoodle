/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.usertours

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name ="tool_usertours_tours")
class ToolUserToursTour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "pathmatch", length = 255)
    String pathMatch

    @Column(name = "enabled", columnDefinition = "bit(1)", nullable = false)
    Boolean enabled = false

    @Column(name = "sortorder", columnDefinition = "bigint(10)", nullable = false)
    Long sortOrder = 0

    @Column(name = "configdata", columnDefinition = "longtext", nullable = false)
    String configData


    @OneToMany(mappedBy = "tour")
    Set<ToolUserToursStep> stepSet

}
