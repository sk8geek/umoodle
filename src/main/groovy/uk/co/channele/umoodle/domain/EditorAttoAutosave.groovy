/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain

import uk.co.channele.umoodle.domain.lib.context.Context
import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="editor_atto_autosave", uniqueConstraints = @UniqueConstraint(columnNames = ["elementid", "contextid", "userid", "pagehash"]))
class EditorAttoAutosave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "elementid", length = 255, nullable = false)
    String elementId

    @Column(name = "pagehash", length = 64, nullable = false)
    String pageHash

    @Column(name = "drafttext", columnDefinition = "longtext", nullable = false)
    String draftText

    @Column(name = "draftid", columnDefinition = "bigint(10)")
    Long draftId

    @Column(name = "pageinstance", length = 64, nullable = false)
    String pageInstance

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0


    @OneToOne()
    @JoinColumn(name = "contextid", columnDefinition = "bigint(10)", nullable = false)
    Context context

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
