/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "course_completion_defaults", uniqueConstraints = @UniqueConstraint(columnNames = ["course", "module"]))
class CourseCompletionDefaults {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long module

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer completion = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer completionview = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer completionusegrade = 0

    @Column(nullable = false)
    Long completionexpected = 0

    @Column(columnDefinition = "longtext")
    String customrules = ""

    @ManyToOne
    @JoinColumn(name = "course", nullable = false)
    Course course

// <KEY NAME="module" TYPE="foreign" FIELDS="module" REFTABLE="modules" REFFIELDS="id"/>
}