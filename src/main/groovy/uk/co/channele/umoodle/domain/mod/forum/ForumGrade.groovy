package uk.co.channele.umoodle.domain.mod.forum

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="forum_grades", uniqueConstraints = @UniqueConstraint(columnNames = ["forum", "itemnumber", "userid"]))
class ForumGrade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "forum", columnDefinition = "bigint(10)", nullable = false)
    Long forum

    @Column(name = "itemnumber", columnDefinition = "bigint(10)", nullable = false)
    Long itemnumber

    @Column(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    Long userid

    @Column(name = "grade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grade

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

//        <KEY NAME="forum" TYPE="foreign" FIELDS="forum" REFTABLE="forum" REFFIELDS="id"/>
}
