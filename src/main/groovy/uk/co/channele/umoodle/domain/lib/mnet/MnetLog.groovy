/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.mnet

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "mnet_log")
class MnetLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long hostid = 0

    @Column(nullable = false)
    Long remoteid = 0

    @Column(nullable = false)
    Long time = 0

    @Column(nullable = false)
    Long userid = 0

    @Column(length = 45, nullable = false)
    String ip = ""

    @Column(nullable = false)
    Long course = 0

    @Column(length = 40, nullable = false)
    String coursename = ""

    @Column(length = 20, nullable = false)
    String module = ""

    @Column(nullable = false)
    Long cmid = 0

    @Column(length = 40, nullable = false)
    String action = ""

    @Column(length = 100, nullable = false)
    String url = ""

    @Column(length = 255, nullable = false)
    String info = ""


// <KEY NAME="primary" TYPE="primary" FIELDS="id" COMMENT="primary key of the mnet_log table"/>

// <INDEX NAME="hostid_userid_course" UNIQUE="false" FIELDS="hostid, userid, course"/>
}
