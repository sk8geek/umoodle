/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.question

import uk.co.channele.umoodle.domain.lib.context.Context
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "question_categories",
        uniqueConstraints = [
                @UniqueConstraint(columnNames = ["contextid", "stamp"]),
                @UniqueConstraint(columnNames = ["contextid", "idnumber"])
        ])
class QuestionCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String info = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer infoformat = 0

    @Column(length = 255, nullable = false)
    String stamp = ""

    @Column(nullable = false)
    Long parent = 0

    @Column(nullable = false)
    Long sortorder = 999

    @Column(length = 100)
    String idnumber = ""


    @ManyToOne
    @JoinColumn(name = "contextid", nullable = false)
    Context context

}
