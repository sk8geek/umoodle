/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.glossary

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="glossary_entries")
class GlossaryEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "concept", length = 255, nullable = false)
    String concept

    @Column(name = "definition", columnDefinition = "longtext", nullable = false)
    String definition

    @Column(name = "definitionformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer definitionFormat = 0

    @Column(name = "definitiontrust", columnDefinition = "tinyint(2)", nullable = false)
    Integer definitionTrust = 0

    @Column(name = "attachment", length = 100, nullable = false)
    String attachment

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "teacherentry", columnDefinition = "tinyint(2)", nullable = false)
    Integer teacherEntry = 0

    @Column(name = "sourceglossaryid", columnDefinition = "bigint(10)", nullable = false)
    Long sourceGlossaryId = 0

    @Column(name = "usedynalink", columnDefinition = "tinyint(2)", nullable = false)
    Integer useDynaLink = 1

    @Column(name = "casesensitive", columnDefinition = "tinyint(2)", nullable = false)
    Integer caseSensitive = 0

    @Column(name = "fullmatch", columnDefinition = "tinyint(2)", nullable = false)
    Integer fullMatch = 1

    @Column(name = "approved", columnDefinition = "tinyint(2)", nullable = false)
    Integer approved = 1


    @OneToMany(mappedBy = "entry")
    Set<GlossaryAlias> aliasSet

    @ManyToOne()
    @JoinColumn(name = "glossaryid", columnDefinition = "bigint(10)", nullable = false)
    Glossary glossary

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
