/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lti

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="lti")
class Lti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext")
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)")
    Integer introformat = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "typeid", columnDefinition = "bigint(10)")
    Long typeid

    @Column(name = "toolurl", columnDefinition = "longtext", nullable = false)
    String toolurl

    @Column(name = "securetoolurl", columnDefinition = "longtext")
    String securetoolurl

    @Column(name = "instructorchoicesendname", columnDefinition = "bit(1)")
    Boolean instructorchoicesendname

    @Column(name = "instructorchoicesendemailaddr", columnDefinition = "bit(1)")
    Boolean instructorchoicesendemailaddr

    @Column(name = "instructorchoiceallowroster", columnDefinition = "bit(1)")
    Boolean instructorchoiceallowroster

    @Column(name = "instructorchoiceallowsetting", columnDefinition = "bit(1)")
    Boolean instructorchoiceallowsetting

    @Column(name = "instructorcustomparameters", length = 255)
    String instructorcustomparameters

    @Column(name = "instructorchoiceacceptgrades", columnDefinition = "bit(1)")
    Boolean instructorchoiceacceptgrades

    @Column(name = "grade", columnDefinition = "bigint(10)", nullable = false)
    Long grade = 100

    @Column(name = "launchcontainer", columnDefinition = "tinyint(2)", nullable = false)
    Integer launchcontainer = 1

    @Column(name = "resourcekey", length = 255)
    String resourcekey

    @Column(name = "password", length = 255)
    String password

    @Column(name = "debuglaunch", columnDefinition = "bit(1)", nullable = false)
    Boolean debuglaunch = false

    @Column(name = "showtitlelaunch", columnDefinition = "bit(1)", nullable = false)
    Boolean showtitlelaunch = false

    @Column(name = "showdescriptionlaunch", columnDefinition = "bit(1)", nullable = false)
    Boolean showdescriptionlaunch = false

    @Column(name = "servicesalt", length = 40)
    String servicesalt

    @Column(name = "icon", columnDefinition = "longtext")
    String icon

    @Column(name = "secureicon", columnDefinition = "longtext")
    String secureicon


    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
