/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.scorm

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="scorm_scoes_track", uniqueConstraints = @UniqueConstraint(columnNames = ["userid", "scormid", "scoid", "attempt", "element"]))
class ScormScoesTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "attempt", columnDefinition = "bigint(10)", nullable = false)
    Long attempt = 1

    @Column(name = "element", length = 255, nullable = false)
    String element

    @Column(name = "value", columnDefinition = "longtext", nullable = false)
    String value

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0


    @OneToOne
    @JoinColumn(name = "scormid", columnDefinition = "bigint(10)", nullable = false)
    Scorm scorm

    @OneToOne
    @JoinColumn(name = "scoid", columnDefinition = "bigint(10)", nullable = false)
    ScormScoes sco

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
