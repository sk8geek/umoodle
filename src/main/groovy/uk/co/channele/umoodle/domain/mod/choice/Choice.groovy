/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.choice

import uk.co.channele.umoodle.domain.lib.course.Course
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="choice")
class Choice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introFormat = 0

    @Column(name = "publish", columnDefinition = "tinyint(2)", nullable = false)
    Integer publish = 0

    @Column(name = "showresults", columnDefinition = "tinyint(2)", nullable = false)
    Integer showResults = 0

    @Column(name = "display", columnDefinition = "smallint(4)", nullable = false)
    Integer display = 0

    @Column(name = "allowupdate", columnDefinition = "tinyint(2)", nullable = false)
    Integer allowUpdate = 0

    @Column(name = "allowmultiple", columnDefinition = "tinyint(2)", nullable = false)
    Integer allowMultiple = 0

    @Column(name = "showunanswered", columnDefinition = "tinyint(2)", nullable = false)
    Integer showUnanswered = 0

    @Column(name = "includeinactive", columnDefinition = "tinyint(2)", nullable = false)
    Integer includeInactive = 1

    @Column(name = "limitanswers", columnDefinition = "tinyint(2)", nullable = false)
    Integer limitAnswers = 0

    @Column(name = "timeopen", columnDefinition = "bigint(10)", nullable = false)
    Long timeOpen = 0

    @Column(name = "timeclose", columnDefinition = "bigint(10)", nullable = false)
    Long timeClose = 0

    @Column(name = "showpreview", columnDefinition = "tinyint(2)", nullable = false)
    Integer showPreview = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "completionsubmit", columnDefinition = "bit(1)", nullable = false)
    Boolean completionSubmit = false


    @OneToMany(mappedBy = "choice")
    Set<ChoiceAnswer> choiceAnswerSet

    @OneToMany(mappedBy = "choice")
    Set<ChoiceOption> choiceOptionSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
