/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.chat

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="chat")
class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "keepdays", columnDefinition = "bigint(11)", nullable = false)
    Long keepdays = 0

    @Column(name = "studentlogs", columnDefinition = "smallint(4)", nullable = false)
    Integer studentlogs = 0

    @Column(name = "chattime", columnDefinition = "bigint(10)", nullable = false)
    Long chattime = 0

    @Column(name = "schedule", columnDefinition = "smallint(4)", nullable = false)
    Integer schedule = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0


    @OneToMany(mappedBy = "chat")
    Set<ChatMessage> messageSet

    @ManyToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}