/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.ddimageortext

import uk.co.channele.umoodle.domain.lib.question.Question

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="qtype_ddimageortext_drops")
class QTypeDdImageOrTextDrops {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "no", columnDefinition = "bigint(10)", nullable = false)
    Long no = 0

    @Column(name = "xleft", columnDefinition = "bigint(10)", nullable = false)
    Long xleft = 0

    @Column(name = "ytop", columnDefinition = "bigint(10)", nullable = false)
    Long ytop = 0

    @Column(name = "choice", columnDefinition = "bigint(10)", nullable = false)
    Long choice = 0

    @Column(name = "label", columnDefinition = "longtext", nullable = false)
    String label


    @ManyToOne()
    @JoinColumn(name = "questionid", columnDefinition = "bigint(10)", nullable = false)
    Question question

}