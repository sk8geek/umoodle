/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.analytics

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "analytics_models")
class AnalyticsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer enabled = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer trained = 0

    @Column(length = 1333)
    String name = ""

    @Column(length = 255, nullable = false)
    String target = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String indicators = ""

    @Column(length = 255)
    String timesplitting = ""

    @Column(length = 255)
    String predictionsprocessor = ""

    @Column(nullable = false)
    Long version

    @Column(columnDefinition = "longtext")
    String contextids = ""

    @Column
    Long timecreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(nullable = false)
    Long usermodified


    @OneToMany(mappedBy = "analyticsModel")
    Set<AnalyticsModelsLog> analyticsModelsLogSet

    @OneToMany(mappedBy = "analyticsModel")
    Set<AnalyticsPrediction> analyticsPredictionSet

}
