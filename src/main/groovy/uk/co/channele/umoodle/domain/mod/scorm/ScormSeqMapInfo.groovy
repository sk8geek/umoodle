/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.scorm

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="scorm_seq_mapinfo", uniqueConstraints = @UniqueConstraint(columnNames = ["scoid", "id", "objectiveid"]))
class ScormSeqMapInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "targetobjectiveid", columnDefinition = "bigint(10)", nullable = false)
    Long targetobjectiveid = 0

    @Column(name = "readsatisfiedstatus", columnDefinition = "bit(1)", nullable = false)
    Boolean readsatisfiedstatus = true

    @Column(name = "readnormalizedmeasure", columnDefinition = "bit(1)", nullable = false)
    Boolean readnormalizedmeasure = true

    @Column(name = "writesatisfiedstatus", columnDefinition = "bit(1)", nullable = false)
    Boolean writesatisfiedstatus = false

    @Column(name = "writenormalizedmeasure", columnDefinition = "bit(1)", nullable = false)
    Boolean writenormalizedmeasure = false


    @OneToOne()
    @JoinColumn(name = "scoid", columnDefinition = "bigint(10)", nullable = false)
    ScormScoes sco

    @OneToOne()
    @JoinColumn(name = "objectiveid", columnDefinition = "bigint(10)", nullable = false)
    ScormSeqObjective seqObjective

}
