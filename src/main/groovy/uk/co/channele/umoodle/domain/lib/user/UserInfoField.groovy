/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.user

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "user_info_field")
class UserInfoField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String shortname = "shortname"

    @Column(columnDefinition = "longtext", nullable = false)
    String name = ""

    @Column(length = 255, nullable = false)
    String datatype = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer descriptionformat = 0

    @Column(nullable = false)
    Long categoryid = 0

    @Column(nullable = false)
    Long sortorder = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer required = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer locked = 0

    @Column(columnDefinition = "smallint", nullable = false)
    Integer visible = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer forceunique = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer signup = 0

    @Column(columnDefinition = "longtext")
    String defaultdata = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer defaultdataformat = 0

    @Column(columnDefinition = "longtext")
    String param1 = ""

    @Column(columnDefinition = "longtext")
    String param2 = ""

    @Column(columnDefinition = "longtext")
    String param3 = ""

    @Column(columnDefinition = "longtext")
    String param4 = ""

    @Column(columnDefinition = "longtext")
    String param5 = ""

}
