/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.data

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="data_fields")
class DataField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "type", length = 255, nullable = false)
    String type

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "description", columnDefinition = "longtext", nullable = false)
    String description

    @Column(name = "required", columnDefinition = "bit(1)", nullable = false)
    Boolean required = false

    @Column(name = "param1", columnDefinition = "longtext")
    String param1

    @Column(name = "param2", columnDefinition = "longtext")
    String param2

    @Column(name = "param3", columnDefinition = "longtext")
    String param3

    @Column(name = "param4", columnDefinition = "longtext")
    String param4

    @Column(name = "param5", columnDefinition = "longtext")
    String param5

    @Column(name = "param6", columnDefinition = "longtext")
    String param6

    @Column(name = "param7", columnDefinition = "longtext")
    String param7

    @Column(name = "param8", columnDefinition = "longtext")
    String param8

    @Column(name = "param9", columnDefinition = "longtext")
    String param9

    @Column(name = "param10", columnDefinition = "longtext")
    String param10


    @ManyToOne()
    @JoinColumn(name = "dataid", columnDefinition = "bigint(10)", nullable = false)
    Data data

}
