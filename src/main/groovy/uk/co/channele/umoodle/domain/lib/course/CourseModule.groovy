/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "course_modules")
class CourseModule {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long module = 0

    @Column(nullable = false)
    Long instance = 0

    @Column(nullable = false)
    Long section = 0

    @Column(length = 100)
    String idnumber = ""

    @Column(nullable = false)
    Long added = 0

    @Column(columnDefinition = "smallint", nullable = false)
    Integer score = 0

    @Column(columnDefinition = "mediumint", nullable = false)
    Integer indent = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visible = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visibleoncoursepage = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visibleold = 1

    @Column(columnDefinition = "smallint", nullable = false)
    Integer groupmode = 0

    @Column(nullable = false)
    Long groupingid = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer completion = 0

    @Column
    Long completiongradeitemnumber

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer completionview = 0

    @Column(nullable = false)
    Long completionexpected = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer showdescription = 0

    @Column(columnDefinition = "longtext")
    String availability = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer deletioninprogress = 0

    @ManyToOne
    @JoinColumn(name = "course", nullable = false)
    Course course

// <KEY NAME="groupingid" TYPE="foreign" FIELDS="groupingid" REFTABLE="groupings" REFFIELDS="id"/>
// <INDEX NAME="idnumber-course" UNIQUE="false" FIELDS="idnumber, course" COMMENT="non unique index (although programatically we are guarantying some sort of uniqueness both under this table and the grade_items one). TODO: We need a central store of module idnumbers in the future."/>
}


