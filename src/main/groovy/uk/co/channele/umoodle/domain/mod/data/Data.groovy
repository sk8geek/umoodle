/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.data

import uk.co.channele.umoodle.domain.lib.course.Course
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="data")
class Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introformat = 0

    @Column(name = "comments", columnDefinition = "smallint(4)", nullable = false)
    Integer comments = 0

    @Column(name = "timeavailablefrom", columnDefinition = "bigint(10)", nullable = false)
    Long timeavailablefrom = 0

    @Column(name = "timeavailableto", columnDefinition = "bigint(10)", nullable = false)
    Long timeavailableto = 0

    @Column(name = "timeviewfrom", columnDefinition = "bigint(10)", nullable = false)
    Long timeviewfrom = 0

    @Column(name = "timeviewto", columnDefinition = "bigint(10)", nullable = false)
    Long timeviewto = 0

    @Column(name = "requiredentries", columnDefinition = "int(8)", nullable = false)
    Long requiredentries = 0

    @Column(name = "requiredentriestoview", columnDefinition = "int(8)", nullable = false)
    Long requiredentriestoview = 0

    @Column(name = "maxentries", columnDefinition = "int(8)", nullable = false)
    Long maxentries = 0

    @Column(name = "rssarticles", columnDefinition = "smallint(4)", nullable = false)
    Integer rssarticles = 0

    @Column(name = "singletemplate", columnDefinition = "longtext")
    String singletemplate

    @Column(name = "listtemplate", columnDefinition = "longtext")
    String listtemplate

    @Column(name = "listtemplateheader", columnDefinition = "longtext")
    String listtemplateheader

    @Column(name = "listtemplatefooter", columnDefinition = "longtext")
    String listtemplatefooter

    @Column(name = "addtemplate", columnDefinition = "longtext")
    String addtemplate

    @Column(name = "rsstemplate", columnDefinition = "longtext")
    String rsstemplate

    @Column(name = "rsstitletemplate", columnDefinition = "longtext")
    String rsstitletemplate

    @Column(name = "csstemplate", columnDefinition = "longtext")
    String csstemplate

    @Column(name = "jstemplate", columnDefinition = "longtext")
    String jstemplate

    @Column(name = "asearchtemplate", columnDefinition = "longtext")
    String asearchtemplate

    @Column(name = "approval", columnDefinition = "smallint(4)", nullable = false)
    Integer approval = 0

    @Column(name = "manageapproved", columnDefinition = "smallint(4)", nullable = false)
    Integer manageapproved = 1

    @Column(name = "scale", columnDefinition = "bigint(10)", nullable = false)
    Long scale = 0

    @Column(name = "assessed", columnDefinition = "bigint(10)", nullable = false)
    Long assessed = 0

    @Column(name = "assesstimestart", columnDefinition = "bigint(10)", nullable = false)
    Long assesstimestart = 0

    @Column(name = "assesstimefinish", columnDefinition = "bigint(10)", nullable = false)
    Long assesstimefinish = 0

    @Column(name = "defaultsort", columnDefinition = "bigint(10)", nullable = false)
    Long defaultsort = 0

    @Column(name = "defaultsortdir", columnDefinition = "smallint(4)", nullable = false)
    Integer defaultsortdir = 0

    @Column(name = "editany", columnDefinition = "smallint(4)", nullable = false)
    Integer editany = 0

    @Column(name = "notification", columnDefinition = "bigint(10)", nullable = false)
    Long notification = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "config", columnDefinition = "longtext")
    String config

    @Column(name = "completionentries", columnDefinition = "bigint(10)")
    Long completionentries = 0


    @OneToMany(mappedBy = "data")
    Set<DataField> fieldSet

    @OneToMany(mappedBy = "data")
    Set<DataRecord> recordSet

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
