/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.question

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "question_response_analysis")
class QuestionResponseAnalysis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 40, nullable = false)
    String hashcode = ""

    @Column(length = 255, nullable = false)
    String whichtries = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column
    Long variant

    @Column(length = 100, nullable = false)
    String subqid = ""

    @Column(length = 100)
    String aid = ""

    @Column(columnDefinition = "longtext")
    String response = ""

    @Column(name = "credit", columnDefinition = "decimal(15,5)", nullable = false)
    BigDecimal credit


    @ManyToOne()
    @JoinColumn(name = "questionid", nullable = false)
    Question question

    @OneToMany(mappedBy = "responseAnalysis")
    Set<QuestionResponseCount> questionResponseCountSet

}