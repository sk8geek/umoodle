/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.file

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "file_conversion")
class FileConversion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(length = 100, nullable = false)
    String targetformat = ""

    @Column()
    Long status = 0

    @Column(columnDefinition = "longtext")
    String statusmessage = ""

    @Column(length = 255)
    String converter = ""

    @Column(columnDefinition = "longtext")
    String data = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @Column(nullable = false)
    Long usermodified

    @OneToOne
    @JoinColumn(name = "sourcefileid", nullable = false)
    File sourceFile

    @OneToOne
    @JoinColumn(name = "destfileid", nullable = false)
    File destFile

}
