/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.question

import uk.co.channele.umoodle.domain.question.type.calculated.QuestionCalculated
import uk.co.channele.umoodle.domain.question.type.calculated.QuestionCalculatedOption
import uk.co.channele.umoodle.domain.question.type.ddimageortext.QTypeDdImageOrText
import uk.co.channele.umoodle.domain.question.type.ddimageortext.QTypeDdImageOrTextDrags
import uk.co.channele.umoodle.domain.question.type.ddimageortext.QTypeDdImageOrTextDrops
import uk.co.channele.umoodle.domain.question.type.ddmarker.QTypeDdMarker
import uk.co.channele.umoodle.domain.question.type.ddmarker.QTypeDdMarkerDrags
import uk.co.channele.umoodle.domain.question.type.ddmarker.QTypeDdMarkerDrops
import uk.co.channele.umoodle.domain.question.type.ddwtos.QuestionDdWto
import uk.co.channele.umoodle.domain.question.type.essay.QTypeEssayOptions
import uk.co.channele.umoodle.domain.question.type.gapselect.QuestionGapSelect
import uk.co.channele.umoodle.domain.question.type.match.QTypeMatchOptions
import uk.co.channele.umoodle.domain.question.type.match.QTypeMatchSubQuestions
import uk.co.channele.umoodle.domain.question.type.multianswer.QuestionMultiAnswer
import uk.co.channele.umoodle.domain.question.type.multichoice.QTypeMultiChoiceOptions
import uk.co.channele.umoodle.domain.question.type.numerical.QuestionNumerical
import uk.co.channele.umoodle.domain.question.type.numerical.QuestionNumericalOptions
import uk.co.channele.umoodle.domain.question.type.numerical.QuestionNumericalUnits
import uk.co.channele.umoodle.domain.question.type.randomsamatch.QTypeRandomsamatchOptions
import uk.co.channele.umoodle.domain.question.type.shortanswer.QTypeShortAnswerOptions
import uk.co.channele.umoodle.domain.question.type.truefalse.QuestionTrueFalse

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "question", uniqueConstraints = @UniqueConstraint(columnNames = ["category", "idnumber"]))
class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long category = 0

    @Column(nullable = false)
    Long parent = 0

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String questiontext = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer questiontextformat = 0

    @Column(columnDefinition = "longtext", nullable = false)
    String generalfeedback = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer generalfeedbackformat = 0

    @Column(columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal defaultmark = 1

    @Column(columnDefinition = "decimal(12,7)", nullable = false)
    BigDecimal penalty

    @Column(length = 20, nullable = false)
    String qtype = ""

    @Column(nullable = false)
    Long length = 1

    @Column(length = 255, nullable = false)
    String stamp = ""

    @Column(length = 255, nullable = false)
    String version = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer hidden = 0

    @Column()
    Long createdby

    @Column()
    Long modifiedby

    @Column(length = 100)
    String idnumber = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "question")
    Set<QTypeDdImageOrText> ddImageOrTextSet

    @OneToMany(mappedBy = "question")
    Set<QTypeDdImageOrTextDrags> ddImageOrTextDragsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeDdImageOrTextDrops> ddImageOrTextDropsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeDdMarker> ddMarkerSet

    @OneToMany(mappedBy = "question")
    Set<QTypeDdMarkerDrags> ddMarkerDragsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeDdMarkerDrops> ddMarkerDropsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionDdWto> ddWtoSet

    @OneToMany(mappedBy = "question")
    Set<QTypeEssayOptions> essayOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionGapSelect> gapSelectSet

    @OneToMany(mappedBy = "question")
    Set<QTypeMatchOptions> matchOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeMatchSubQuestions> matchSubQuestionsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionMultiAnswer> multiAnswerSet

    @OneToMany(mappedBy = "question")
    Set<QTypeMultiChoiceOptions> multiChoiceOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionNumerical> numericalSet

    @OneToMany(mappedBy = "question")
    Set<QuestionNumericalOptions> numericalOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionNumericalUnits> numericalUnitsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeRandomsamatchOptions> randomsamatchOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QTypeShortAnswerOptions> shortAnswerOptionsSet

    @OneToMany(mappedBy = "question")
    Set<QuestionTrueFalse> trueFalseSet





    @OneToMany(mappedBy = "question")
    Set<QuestionCalculated> questionCalculatedSet

    @OneToMany(mappedBy = "question")
    Set<QuestionCalculatedOption> questionCalculatedOptionSet



// <KEY NAME="category" TYPE="foreign" FIELDS="category" REFTABLE="question_categories" REFFIELDS="id"/>
// <KEY NAME="parent" TYPE="foreign" FIELDS="parent" REFTABLE="question" REFFIELDS="id" COMMENT="note that to make this recursive FK working someday, the parent field must be declared NULL"/>
// <KEY NAME="createdby" TYPE="foreign" FIELDS="createdby" REFTABLE="user" REFFIELDS="id" COMMENT="foreign (createdby) references user (id)"/>
// <KEY NAME="modifiedby" TYPE="foreign" FIELDS="modifiedby" REFTABLE="user" REFFIELDS="id" COMMENT="foreign (modifiedby) references user (id)"/>

}
