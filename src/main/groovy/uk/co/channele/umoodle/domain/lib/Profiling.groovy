/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "profiling")
class Profiling {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 32, nullable = false)
    String runid = ""

    @Column(length = 255, nullable = false)
    String url = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String data = ""

    @Column(nullable = false)
    Long totalexecutiontime

    @Column(nullable = false)
    Long totalcputime

    @Column(nullable = false)
    Long totalcalls

    @Column(nullable = false)
    Long totalmemory

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer runreference = 0

    @Column(length = 255, nullable = false)
    String runcomment = ""

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
// <KEY NAME="runid_uk" TYPE="unique" FIELDS="runid"/>

// <INDEX NAME="url_runreference_ix" UNIQUE="false" FIELDS="url, runreference"/>
// <INDEX NAME="timecreated_runreference_ix" UNIQUE="false" FIELDS="timecreated, runreference"/>
}
