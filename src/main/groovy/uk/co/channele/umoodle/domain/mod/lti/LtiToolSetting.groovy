/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lti

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="lti_tool_settings")
class LtiToolSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "settings", columnDefinition = "longtext", nullable = false)
    String settings

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified


    @ManyToOne()
    @JoinColumn(name = "toolproxyid", columnDefinition = "bigint(10)", nullable = false)
    LtiToolProxy ltiToolProxy

    @OneToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)")
    Course course

    @OneToOne()
    @JoinColumn(name = "coursemoduleid", columnDefinition = "bigint(10)")
    Lti courseModule

    // TODO: Moodle 3.8
//        <KEY NAME="typeid" TYPE="foreign" FIELDS="typeid" REFTABLE="lti_types" REFFIELDS="id"/>

}