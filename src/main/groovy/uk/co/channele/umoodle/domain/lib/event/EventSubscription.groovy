/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.event

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "event_subscriptions")
class EventSubscription {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String url = ""

    @Column(nullable = false)
    Long categoryid = 0

    @Column(nullable = false)
    Long courseid = 0

    @Column(nullable = false)
    Long groupid = 0

    @Column(nullable = false)
    Long userid = 0

    @Column(length = 20, nullable = false)
    String eventtype = ""

    @Column(nullable = false)
    Long pollinterval = 0

    @Column
    Long lastupdated

    @Column(length = 255, nullable = false)
    String name = ""

    @OneToMany(mappedBy = "eventSubscription")
    Set<Event> eventSet

// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
}
