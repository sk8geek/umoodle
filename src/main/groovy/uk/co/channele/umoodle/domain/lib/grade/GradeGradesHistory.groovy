/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.grade

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "grade_grades_history", uniqueConstraints = @UniqueConstraint(columnNames = ["userid", "itemid", "timemodified"]))
class GradeGradesHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long action = 0

    @Column(length = 255)
    String source = ""

    @Column(name = "timemodified")
    Long timeModified

    @Column(name = "rawgrade", columnDefinition = "decimal(10,5)")
    BigDecimal rawGrade

    @Column(name = "rawgrademax", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal rawGradeMax

    @Column(name = "rawgrademin", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal rawGradeMin

    @Column(name = "finalgrade", columnDefinition = "decimal(10,5)")
    BigDecimal finalGrade

    @Column(nullable = false)
    Long hidden = 0

    @Column(nullable = false)
    Long locked = 0

    @Column(name = "locktime", nullable = false)
    Long lockTime = 0

    @Column(nullable = false)
    Long exported = 0

    @Column(nullable = false)
    Long overridden = 0

    @Column(nullable = false)
    Long excluded = 0

    @Column(columnDefinition = "longtext")
    String feedback = ""

    @Column(name = "feedbackformat", nullable = false)
    Long feedbackFormat = 0

    @Column(columnDefinition = "longtext")
    String information = ""

    @Column(name = "informationformat", nullable = false)
    Long informationFormat = 0


    @OneToOne
    @JoinColumn(name = "itemid", nullable = false)
    GradeItem gradeItem

    @OneToOne
    @JoinColumn(name = "oldid", nullable = false)
    GradeGrades oldId

    @OneToOne
    @JoinColumn(name = "rawscaleid")
    Scale scale

    @OneToOne
    @JoinColumn(name = "usermodified")
    User userModified

    @OneToOne
    @JoinColumn(name = "loggeduser")
    User loggedUser

    @OneToOne
    @JoinColumn(name = "userid")
    User user

}