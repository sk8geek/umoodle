/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.competency

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "competency")
class Competency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 100)
    String shortname = ""

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer descriptionformat = 0

    @Column(length = 100)
    String idnumber = ""

    @Column(nullable = false)
    Long parentid = 0

    @Column(length = 255, nullable = false)
    String path = ""

    @Column(nullable = false)
    Long sortorder

    @Column(length = 100)
    String ruletype = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer ruleoutcome = 0

    @Column(columnDefinition = "longtext")
    String ruleconfig = ""

    @Column()
    Long scaleid

    @Column(columnDefinition = "longtext")
    String scaleconfiguration = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column()
    Long usermodified

    @Column(name = "timecreated", nullable = false)
    Long timeCreated


    @OneToMany(mappedBy = "competency")
    Set<CompetencyCourseComp> courseCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyModuleComp> moduleCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyPlanComp> planCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyRelatedComp> relatedCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyTemplateComp> templateCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyUserComp> userCompSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyUserCompCourse> userCompCourseSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyUserCompPlan> userCompPlanSet

    @OneToMany(mappedBy = "competency")
    Set<CompetencyUserEvidenceComp> userEvidenceCompSet

    @ManyToOne
    @JoinColumn(name = "competencyframeworkid")
    CompetencyFramework competencyFramework

}
