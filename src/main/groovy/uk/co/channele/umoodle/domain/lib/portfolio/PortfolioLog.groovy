/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.portfolio

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "portfolio_log")
class PortfolioLog {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long userid

    @Column(nullable = false)
    Long time

    @Column(nullable = false)
    Long portfolio

    @Column(length = 150, nullable = false)
    String caller_class = ""

    @Column(length = 255, nullable = false)
    String caller_file = ""

    @Column(length = 255)
    String caller_component = ""

    @Column(length = 255, nullable = false)
    String caller_sha1 = ""

    @Column(nullable = false)
    Long tempdataid = 0

    @Column(length = 255, nullable = false)
    String returnurl = ""

    @Column(length = 255, nullable = false)
    String continueurl = ""


// <KEY NAME="userfk" TYPE="foreign" FIELDS="userid" REFTABLE="user" REFFIELDS="id" COMMENT="fk to user table"/>
// <KEY NAME="portfoliofk" TYPE="foreign" FIELDS="portfolio" REFTABLE="portfolio_instance" REFFIELDS="id" COMMENT="fk to portfolio_instance"/>
}
