/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.customlang

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="tool_customlang", uniqueConstraints = @UniqueConstraint(columnNames = ["lang", "componentid", "stringid"]))
class ToolCustomLang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "lang", length = 20, nullable = false)
    String lang

    @Column(name = "stringid", length = 255, nullable = false)
    String stringId

    @Column(name = "original", columnDefinition = "longtext", nullable = false)
    String original

    @Column(name = "master", columnDefinition = "longtext")
    String master

    @Column(name = "local", columnDefinition = "longtext")
    String local

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified

    @Column(name = "timecustomized", columnDefinition = "bigint(10)")
    Long timeCustomized

    @Column(name = "outdated", columnDefinition = "smallint(3)")
    Integer outdated = 0

    @Column(name = "modified", columnDefinition = "smallint(3)")
    Integer modified = 0


    @ManyToOne()
    @JoinColumn(name = "componentid", columnDefinition = "bigint(10)", nullable = false)
    ToolCustomLangComponent component

}
