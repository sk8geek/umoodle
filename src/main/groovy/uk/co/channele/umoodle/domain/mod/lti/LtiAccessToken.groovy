package uk.co.channele.umoodle.domain.mod.lti

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="lti_access_tokens")
class LtiAccessToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "typeid", columnDefinition = "bigint(10)", nullable = false)
    Long typeid

    @Column(name = "scope", columnDefinition = "longtext", nullable = false)
    String scope

    @Column(name = "token", length = 128, nullable = false, unique = true)
    String token

    @Column(name = "validuntil", columnDefinition = "bigint(10)", nullable = false)
    Long validuntil

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "lastaccess", columnDefinition = "bigint(10)")
    Long lastaccess


//        <KEY NAME="typeid" TYPE="foreign" FIELDS="typeid" REFTABLE="lti_types" REFFIELDS="id"/>

}
