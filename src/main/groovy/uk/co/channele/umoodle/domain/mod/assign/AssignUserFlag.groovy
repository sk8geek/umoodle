/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.assign

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="assign_user_flags")
class AssignUserFlag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "locked", columnDefinition = "bigint(10)", nullable = false)
    Long locked = 0

    @Column(name = "mailed", columnDefinition = "smallint(4)", nullable = false)
    Integer mailed = 0

    @Column(name = "extensionduedate", columnDefinition = "bigint(10)", nullable = false)
    Long extensionduedate = 0

    @Column(name = "workflowstate", length = 20)
    String workflowstate

    @Column(name = "allocatedmarker", columnDefinition = "bigint(10)", nullable = false)
    Long allocatedmarker = 0


    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "assignment", columnDefinition = "bigint(10)", nullable = false)
    Assign assignment

}
