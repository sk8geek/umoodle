/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.quiz

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="quiz_attempts", uniqueConstraints = @UniqueConstraint(columnNames = ["quiz", "userid", "attempt"]))
class QuizAttempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "attempt", columnDefinition = "mediumint(6)", nullable = false)
    Integer attempt = 0

    @Column(name = "uniqueid", columnDefinition = "bigint(10)", nullable = false)
    Long uniqueid = 0

    @Column(name = "layout", columnDefinition = "longtext", nullable = false)
    String layout

    @Column(name = "currentpage", columnDefinition = "bigint(10)", nullable = false)
    Long currentpage = 0

    @Column(name = "preview", columnDefinition = "smallint(3)", nullable = false)
    Integer preview = 0

    @Column(name = "state", length = 16, nullable = false)
    String state

    @Column(name = "timestart", columnDefinition = "bigint(10)", nullable = false)
    Long timestart = 0

    @Column(name = "timefinish", columnDefinition = "bigint(10)", nullable = false)
    Long timefinish = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "timemodifiedoffline", columnDefinition = "bigint(10)", nullable = false)
    Long timemodifiedoffline = 0

    @Column(name = "timecheckstate", columnDefinition = "bigint(10)")
    Long timecheckstate = 0

    @Column(name = "sumgrades", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal sumgrades


    @ManyToOne()
    @JoinColumn(name = "quiz", columnDefinition = "bigint(10)", nullable = false)
    Quiz quiz

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

}
