/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.policy

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="tool_policy_acceptances", uniqueConstraints = @UniqueConstraint(columnNames = ["policyversionid", "userid"]))
class ToolPolicyAcceptances {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "status", columnDefinition = "bit(1)")
    Boolean status

    @Column(name = "lang", length = 30, nullable = false)
    String lang

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "note", columnDefinition = "longtext")
    String note


    @OneToOne()
    @JoinColumn(name = "policyversionid", columnDefinition = "bigint(10)", nullable = false)
    ToolPolicyVersion policyVersion

    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    User modifiedBy

}
