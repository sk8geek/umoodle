/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.file

import uk.co.channele.umoodle.domain.lib.repository.RepositoryInstance

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

/**
 * The combination of repositoryid and reference field is supposed to be a unique identification of an external file.
 * Because the reference is a TEXT field, we can't use to compose the index. So we use the referencehash instead and
 * the file API is responsible to keep it up-to-date"
 *
 */
@Entity
@Table(name = "files_reference", uniqueConstraints = @UniqueConstraint(columnNames = ["referencehash", "repositoryid"]))
class FilesReference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "lastsync")
    Long lastSync

    @Column(columnDefinition = "longtext")
    String reference = ""

    @Column(name = "referencehash", length = 40, nullable = false)
    String referenceHash = ""

    @OneToMany(mappedBy = "referenceFile")
    Set<File> fileSet

    @OneToOne
    @JoinColumn(name = "repositoryid", nullable = false)
    RepositoryInstance repositoryInstance

// <KEY NAME="repositoryid" TYPE="foreign" FIELDS="repositoryid" REFTABLE="repository_instances" REFFIELDS="id"/>
}