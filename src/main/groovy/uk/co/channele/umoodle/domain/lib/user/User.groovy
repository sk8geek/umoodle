/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.user

import uk.co.channele.umoodle.domain.admin.tool.dataprivacy.ToolDataPrivacyRequest
import uk.co.channele.umoodle.domain.blocks.BlockRecentActivity
import uk.co.channele.umoodle.domain.lib.analytics.AnalyticsPredictionActions
import uk.co.channele.umoodle.domain.lib.badge.Badge
import uk.co.channele.umoodle.domain.lib.badge.BadgeBackpack
import uk.co.channele.umoodle.domain.lib.competency.CompetencyPlan
import uk.co.channele.umoodle.domain.lib.competency.CompetencyUserComp
import uk.co.channele.umoodle.domain.lib.competency.CompetencyUserCompCourse
import uk.co.channele.umoodle.domain.lib.competency.CompetencyUserCompPlan
import uk.co.channele.umoodle.domain.lib.competency.CompetencyUserEvidence
import uk.co.channele.umoodle.domain.lib.context.Comment
import uk.co.channele.umoodle.domain.lib.grade.Scale
import uk.co.channele.umoodle.domain.lib.grade.ScaleHistory
import uk.co.channele.umoodle.domain.lib.message.Notification
import uk.co.channele.umoodle.domain.mod.assign.AssignGrade
import uk.co.channele.umoodle.domain.mod.chat.ChatMessage
import uk.co.channele.umoodle.domain.mod.feedback.FeedbackCompleted
import uk.co.channele.umoodle.domain.mod.glossary.Glossary
import uk.co.channele.umoodle.domain.mod.lesson.LessonAttempt

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = ["mnethostid", "username"]))
class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 20, nullable = false)
    String auth = "manual"

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer confirmed = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer policyagreed = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer deleted = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer suspended = 0

    @Column(nullable = false)
    Long mnethostid = 0

    @Column(length = 100, nullable = false)
    String username = ""

    @Column(length = 255, nullable = false)
    String password = ""

    @Column(length = 255, nullable = false)
    String idnumber = ""

    @Column(length = 100, nullable = false)
    String firstname = ""

    @Column(length = 100, nullable = false)
    String lastname = ""

    @Column(length = 100, nullable = false)
    String email = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer emailstop = 0

    @Column(length = 15, nullable = false)
    String icq = ""

    @Column(length = 50, nullable = false)
    String skype = ""

    @Column(length = 50, nullable = false)
    String yahoo = ""

    @Column(length = 50, nullable = false)
    String aim = ""

    @Column(length = 50, nullable = false)
    String msn = ""

    @Column(length = 20, nullable = false)
    String phone1 = ""

    @Column(length = 20, nullable = false)
    String phone2 = ""

    @Column(length = 255, nullable = false)
    String institution = ""

    @Column(length = 255, nullable = false)
    String department = ""

    @Column(length = 255, nullable = false)
    String address = ""

    @Column(length = 120, nullable = false)
    String city = ""

    @Column(length = 2, nullable = false)
    String country = ""

    @Column(length = 30, nullable = false)
    String lang = "en"

    @Column(name = "calendartype", length = 30, nullable = false)
    String calendarType = "gregorian"

    @Column(length = 50, nullable = false)
    String theme = ""

    @Column(length = 100, nullable = false)
    String timezone = "99"

    @Column(name = "firstaccess", nullable = false)
    Long firstAccess = 0

    @Column(name = "lastaccess", nullable = false)
    Long lastAccess = 0

    @Column(name = "lastlogin", nullable = false)
    Long lastLogin = 0

    @Column(name = "currentlogin", nullable = false)
    Long currentLogin = 0

    @Column(name = "lastip", length = 45, nullable = false)
    String lastIp = ""

    @Column(length = 15, nullable = false)
    String secret = ""

    @Column(nullable = false)
    Long picture = 0

    @Column(length = 255, nullable = false)
    String url = ""

    @Column(columnDefinition = "longtext")
    String description

    @Column(name = "descriptionformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer descriptionFormat = 2

    @Column(name = "mailformat", columnDefinition = "bit(1)", nullable = false)
    Integer mailFormat = 1

    @Column(name = "maildigest", columnDefinition = "bit(1)", nullable = false)
    Integer mailDigest = 0

    @Column(name = "maildisplay", columnDefinition = "tinyint(2)", nullable = false)
    Integer mailDisplay = 2

    @Column(name = "autosubscribe", columnDefinition = "bit(1)", nullable = false)
    Integer autoSubscribe = 1

    @Column(name = "trackforums", columnDefinition = "bit(1)", nullable = false)
    Integer trackForums = 0

    @Column(name = "trustbitmask", nullable = false)
    Long trustBitmask = 0

    @Column(name = "imagealt", length = 255)
    String imageAlt

    @Column(name = "lastnamephonetic", length = 255)
    String lastNamePhonetic

    @Column(name = "firstnamephonetic", length = 255)
    String firstNamePhonetic

    @Column(name = "middlename", length = 255)
    String middleName

    @Column(name = "alternatename", length = 255)
    String alternateName

    @Column(name = "timecreated", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified


    @OneToMany(mappedBy = "user")
    Set<AssignGrade> assignGradeSet

    @OneToMany(mappedBy = "user")
    Set<AnalyticsPredictionActions> analyticsPredictionActionsSet

    @OneToMany(mappedBy = "user")
    Set<BackupController> backupControllersSet

    @OneToMany(mappedBy = "user")
    Set<BadgeBackpack> badgeBackpackSet

    @OneToMany(mappedBy = "userCreated")
    Set<Badge> badgeSet

    @OneToMany(mappedBy = "user")
    Set<BlockRecentActivity> blockRecentActivitySet

    @OneToMany(mappedBy = "user")
    Set<ChatMessage> chatMessageSet

    @OneToMany(mappedBy = "user")
    Set<Comment> commentSet

    @OneToMany(mappedBy = "user")
    Set<CompetencyPlan> competencyPlanSet

    @OneToMany(mappedBy = "user")
    Set<CompetencyUserCompCourse> competencyUserCompCourseSet

    @OneToMany(mappedBy = "user")
    Set<CompetencyUserCompPlan> competencyUserCompPlanSet

    @OneToMany(mappedBy = "user")
    Set<CompetencyUserEvidence> competencyUserEvidenceSet

    @OneToMany(mappedBy = "user")
    Set<CompetencyUserComp> competencyUserCompSet

    @OneToMany(mappedBy = "user")
    Set<FeedbackCompleted> feedbackCompletedSet

    @OneToMany(mappedBy = "user")
    Set<LessonAttempt> lessonAttemptSet

    @OneToMany(mappedBy = "fromUser")
    Set<Notification> sentNotificationSet

    @OneToMany(mappedBy = "toUser")
    Set<Notification> receivedNotificationSet

    @OneToMany(mappedBy = "user")
    Set<Scale> scaleSet

    @OneToMany(mappedBy = "user")
    Set<ScaleHistory> scaleHistorySet

    @OneToMany(mappedBy = "user")
    Set<Session> sessionSet

    @OneToMany(mappedBy = "user")
    Set<ToolDataPrivacyRequest> privacyRequestSet

    @OneToMany(mappedBy = "user")
    Set<UserDevice> userDeviceSet

    @OneToMany(mappedBy = "user")
    Set<UserPasswordHistory> userPasswordHistorySet

    @OneToMany(mappedBy = "user")
    Set<UserPrivateKey> userPrivateKeySet


}
