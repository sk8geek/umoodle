/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.tag

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "tag", uniqueConstraints = @UniqueConstraint(columnNames = ["tagcollid", "name"]))
class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(length = 255, nullable = false)
    String rawname = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer isstandard = 0

    @Column(columnDefinition = "longtext")
    String description = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer descriptionformat = 0

    @Column(columnDefinition = "smallint")
    Integer flag = 0

    @Column
    Long timemodified


    @OneToMany(mappedBy = "tag")
    Set<TagCorrelation> tagCorrelationSet

    @OneToMany(mappedBy = "tag")
    Set<TagInstance> tagInstanceSet

    @ManyToOne
    @JoinColumn(name = "tagcollid", referencedColumnName = "id")
    TagColl tagColl

    @ManyToOne
    @JoinColumn(name = "userid", nullable = false)
    User user

}
