/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.calculated

import uk.co.channele.umoodle.domain.lib.question.Question
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="question_calculated_options")
class QuestionCalculatedOption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "synchronize", columnDefinition = "tinyint(2)", nullable = false)
    Integer synchronize = 0

    @Column(name = "single", columnDefinition = "smallint(4)", nullable = false)
    Integer single = 0

    @Column(name = "shuffleanswers", columnDefinition = "smallint(4)", nullable = false)
    Integer shuffleAnswers = 0

    @Column(name = "correctfeedback", columnDefinition = "longtext")
    String correctFeedback

    @Column(name = "correctfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer correctFeedbackFormat = 0

    @Column(name = "partiallycorrectfeedback", columnDefinition = "longtext")
    String partiallyCorrectFeedback

    @Column(name = "partiallycorrectfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer partiallyCorrectFeedbackFormat = 0

    @Column(name = "incorrectfeedback", columnDefinition = "longtext")
    String incorrectFeedback

    @Column(name = "incorrectfeedbackformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer incorrectFeedbackFormat = 0

    @Column(name = "answernumbering", length = 10, nullable = false)
    String answerNumbering

    @Column(name = "shownumcorrect", columnDefinition = "tinyint(2)", nullable = false)
    Integer showNumCorrect = 0


    @ManyToOne()
    @JoinColumn(name = "question", columnDefinition = "bigint(10)", nullable = false)
    Question question

}