/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.enrol.lti

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="enrol_lti_users")
class EnrolLtiUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "serviceurl", columnDefinition = "longtext")
    String serviceurl

    @Column(name = "sourceid", columnDefinition = "longtext")
    String sourceid

    @Column(name = "consumerkey", columnDefinition = "longtext")
    String consumerkey

    @Column(name = "consumersecret", columnDefinition = "longtext")
    String consumersecret

    @Column(name = "membershipsurl", columnDefinition = "longtext")
    String membershipsurl

    @Column(name = "membershipsid", columnDefinition = "longtext")
    String membershipsid

    @Column(name = "lastgrade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal lastgrade

    @Column(name = "lastaccess", columnDefinition = "bigint(10)")
    Long lastaccess

    @Column(name = "timecreated", columnDefinition = "bigint(10)")
    Long timecreated


    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user

    @OneToOne()
    @JoinColumn(name = "toolid", columnDefinition = "bigint(10)", nullable = false)
    EnrolLtiTool ltiTool

}
