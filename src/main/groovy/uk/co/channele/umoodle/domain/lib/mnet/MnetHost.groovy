/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.mnet

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "mnet_host")
class MnetHost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer deleted = 0

    @Column(length = 255, nullable = false)
    String wwwroot = ""

    @Column(length = 45, nullable = false)
    String ip_address = ""

    @Column(length = 80, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String public_key = ""

    @Column(nullable = false)
    Long public_key_expires = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer transport = 0

    @Column(columnDefinition = "mediumint", nullable = false)
    Integer portno = 0

    @Column(nullable = false)
    Long last_connect_time = 0

    @Column(nullable = false)
    Long last_log_id = 0

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer force_theme = 0

    @Column(length = 100)
    String theme = ""

    @Column(nullable = false)
    Long applicationid = 1

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer sslverification = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id" COMMENT="primary key of the mnet_host table"/>
// <KEY NAME="applicationid" TYPE="foreign" FIELDS="applicationid" REFTABLE="mnet_application" REFFIELDS="id"/>
}
