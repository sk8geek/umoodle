/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.tag

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "tag_coll")
class TagColl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 255)
    String name = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer isdefault = 0

    @Column(length = 100)
    String component = ""

    @Column(columnDefinition = "mediumint", nullable = false)
    Integer sortorder = 0

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer searchable = 1

    @Column(length = 255)
    String customurl = ""

    @OneToMany(mappedBy = "tagColl")
    Set<Tag> tagSet

    @OneToMany(mappedBy = "tagColl")
    Set<TagArea> tagAreaSet

}
