/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.calculated

import uk.co.channele.umoodle.domain.lib.question.Question
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="question_calculated")
class QuestionCalculated {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "answer", columnDefinition = "bigint(10)", nullable = false)
    Long answer = 0

    @Column(name = "tolerance", length = 20, nullable = false)
    String tolerance

    @Column(name = "tolerancetype", columnDefinition = "bigint(10)", nullable = false)
    Long tolerancetype = 1

    @Column(name = "correctanswerlength", columnDefinition = "bigint(10)", nullable = false)
    Long correctanswerlength = 2

    @Column(name = "correctanswerformat", columnDefinition = "bigint(10)", nullable = false)
    Long correctanswerformat = 2


    @ManyToOne()
    @JoinColumn(name = "question", columnDefinition = "bigint(10)", nullable = false)
    Question question

}
