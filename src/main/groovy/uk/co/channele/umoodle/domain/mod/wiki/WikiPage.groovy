/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.wiki

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name ="wiki_pages", uniqueConstraints = @UniqueConstraint(columnNames = ["subwikiid", "title", "userid"]))
class WikiPage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "subwikiid", columnDefinition = "bigint(10)", nullable = false)
    Long subwikiid = 0

    @Column(name = "title", length = 255, nullable = false)
    String title

    @Column(name = "cachedcontent", columnDefinition = "longtext", nullable = false)
    String cachedcontent

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "timerendered", columnDefinition = "bigint(10)", nullable = false)
    Long timerendered = 0

    @Column(name = "pageviews", columnDefinition = "bigint(10)", nullable = false)
    Long pageviews = 0

    @Column(name = "readonly", columnDefinition = "bit(1)", nullable = false)
    Boolean readonly = false


    @OneToOne()
    @JoinColumn(name = "userid", columnDefinition = "bigint(10)", nullable = false)
    User user



//        <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
//        <KEY NAME="subwikititleuser" TYPE="unique" FIELDS=/>
//        <KEY NAME="subwikifk" TYPE="foreign" FIELDS="subwikiid" REFTABLE="wiki_subwikis" REFFIELDS="id" COMMENT="Foreign key to subwiki table"/>

}
