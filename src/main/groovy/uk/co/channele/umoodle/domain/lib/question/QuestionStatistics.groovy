/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.question

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "question_statistics")
class QuestionStatistics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 40, nullable = false)
    String hashcode = ""

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column
    Long slot

    @Column(columnDefinition = "smallint", nullable = false)
    Integer subquestion

    @Column
    Long variant

    @Column(nullable = false)
    Long s = 0

    @Column(columnDefinition = "decimal(15,5)")
    BigDecimal effectiveweight

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer negcovar = 0

    @Column(columnDefinition = "decimal(15,5)")
    BigDecimal discriminationindex

    @Column(columnDefinition = "decimal(15,5)")
    BigDecimal discriminativeefficiency

    @Column(columnDefinition = "decimal(15,5)")
    BigDecimal sd

    @Column(columnDefinition = "decimal(15,5)")
    BigDecimal facility

    @Column(columnDefinition = "longtext")
    String subquestions = ""

    @Column(columnDefinition = "decimal(12,7)")
    BigDecimal maxmark

    @Column(columnDefinition = "longtext")
    String positions = ""

    @Column(columnDefinition = "decimal(12,7)")
    BigDecimal randomguessscore


    @ManyToOne()
    @JoinColumn(name = "questionid", nullable = false)
    Question question

}
