/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.admin.tool.policy

import uk.co.channele.umoodle.domain.lib.user.User

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="tool_policy_versions")
class ToolPolicyVersion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 1333, nullable = false)
    String name

    @Column(name = "type", columnDefinition = "smallint(3)", nullable = false)
    Integer type = 0

    @Column(name = "audience", columnDefinition = "smallint(3)", nullable = false)
    Integer audience = 0

    @Column(name = "archived", columnDefinition = "smallint(3)", nullable = false)
    Integer archived = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timeCreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified

    @Column(name = "agreementstyle", columnDefinition = "smallint(3)", nullable = false)
    Integer agreementStyle = 0

    @Column(name = "optional", columnDefinition = "smallint(3)", nullable = false)
    Integer optional = 0

    @Column(name = "revision", length = 1333, nullable = false)
    String revision

    @Column(name = "summary", columnDefinition = "longtext", nullable = false)
    String summary

    @Column(name = "summaryformat", columnDefinition = "smallint(3)", nullable = false)
    Integer summaryFormat

    @Column(name = "content", columnDefinition = "longtext", nullable = false)
    String content

    @Column(name = "contentformat", columnDefinition = "smallint(3)", nullable = false)
    Integer contentFormat


    @OneToOne()
    @JoinColumn(name = "policyid", columnDefinition = "bigint(10)", nullable = false)
    ToolPolicy policy

    @OneToOne()
    @JoinColumn(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    User modifiedBy

}