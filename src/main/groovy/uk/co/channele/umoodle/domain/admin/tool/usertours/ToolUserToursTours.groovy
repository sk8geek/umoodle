package uk.co.channele.umoodle.domain.admin.tool.usertours

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="tool_usertours_tours")
class ToolUserToursTours {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "description", columnDefinition = "longtext")
    String description

    @Column(name = "pathmatch", length = 255)
    String pathmatch

    @Column(name = "enabled", columnDefinition = "bit(1)", nullable = false)
    Boolean enabled = false

    @Column(name = "sortorder", columnDefinition = "bigint(10)", nullable = false)
    Long sortorder = 0

    @Column(name = "configdata", columnDefinition = "longtext", nullable = false)
    String configdata

}
