/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.context

import uk.co.channele.umoodle.domain.lib.analytics.AnalyticsIndicatorCalc
import uk.co.channele.umoodle.domain.lib.analytics.AnalyticsPrediction
import uk.co.channele.umoodle.domain.lib.block.BlockInstances
import uk.co.channele.umoodle.domain.lib.blog.BlogAssociation
import uk.co.channele.umoodle.domain.lib.competency.CompetencyFramework

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "context", uniqueConstraints = @UniqueConstraint(columnNames = ["contextlevel", "instanceid"]))
class Context {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "contextlevel", nullable = false)
    Long contextlevel = 0

    @Column(name = "instanceid", nullable = false)
    Long instanceid = 0

    @Column(name = "path", length = 255)
    String path = ""

    @Column(name = "depth", columnDefinition = "tinyint", nullable = false)
    Integer depth = 0    // NOTE OneToMany relationships

    @Column(name = "locked", columnDefinition = "tinyint", nullable = false)
    Integer locked = 0


    @OneToMany(mappedBy = "context")
    Set<AnalyticsPrediction> analyticsPredictionSet

    @OneToMany(mappedBy = "context")
    Set<AnalyticsIndicatorCalc> analyticsIndicatorCalcSet

    @OneToMany(mappedBy = "context")
    Set<BlogAssociation> blogAssociationSet

    @OneToMany(mappedBy = "context")
    Set<CompetencyFramework> competencyFrameworkSet

    @OneToMany(mappedBy = "context")
    Set<GradingArea> gradingAreaSet

}