package uk.co.channele.umoodle.domain.admin.tool.policy

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name ="tool_policy_versions")
class ToolPolicyVersions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 1333, nullable = false)
    String name

    @Column(name = "type", columnDefinition = "smallint(3)", nullable = false)
    Integer type = 0

    @Column(name = "audience", columnDefinition = "smallint(3)", nullable = false)
    Integer audience = 0

    @Column(name = "archived", columnDefinition = "smallint(3)", nullable = false)
    Integer archived = 0

    @Column(name = "usermodified", columnDefinition = "bigint(10)", nullable = false)
    Long usermodified

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified

    @Column(name = "policyid", columnDefinition = "bigint(10)", nullable = false)
    Long policyid

    @Column(name = "agreementstyle", columnDefinition = "smallint(3)", nullable = false)
    Integer agreementstyle = 0

    @Column(name = "optional", columnDefinition = "smallint(3)", nullable = false)
    Integer optional = 0

    @Column(name = "revision", length = 1333, nullable = false)
    String revision

    @Column(name = "summary", columnDefinition = "longtext", nullable = false)
    String summary

    @Column(name = "summaryformat", columnDefinition = "smallint(3)", nullable = false)
    Integer summaryformat

    @Column(name = "content", columnDefinition = "longtext", nullable = false)
    String content

    @Column(name = "contentformat", columnDefinition = "smallint(3)", nullable = false)
    Integer contentformat


//        <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
//        <KEY NAME="fk_usermodified" TYPE="foreign" FIELDS="usermodified" REFTABLE="user" REFFIELDS="id"/>
//        <KEY NAME="fk_policyid" TYPE="foreign" FIELDS="policyid" REFTABLE="tool_policy" REFFIELDS="id"/>

}