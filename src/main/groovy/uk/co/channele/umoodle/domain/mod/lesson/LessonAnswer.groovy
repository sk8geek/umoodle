/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.lesson

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="lesson_answers")
class LessonAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "jumpto", columnDefinition = "bigint(11)", nullable = false)
    Long jumpto = 0

    @Column(name = "grade", columnDefinition = "smallint(4)", nullable = false)
    Integer grade = 0

    @Column(name = "score", columnDefinition = "bigint(10)", nullable = false)
    Long score = 0

    @Column(name = "flags", columnDefinition = "smallint(3)", nullable = false)
    Integer flags = 0

    @Column(name = "timecreated", columnDefinition = "bigint(10)", nullable = false)
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timemodified = 0

    @Column(name = "answer", columnDefinition = "longtext")
    String answer

    @Column(name = "answerformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer answerformat = 0

    @Column(name = "response", columnDefinition = "longtext")
    String response

    @Column(name = "responseformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer responseformat = 0


    @ManyToOne()
    @JoinColumn(name = "lessonid", columnDefinition = "bigint(10)", nullable = false)
    Lesson lesson

    @OneToOne()
    @JoinColumn(name = "pageid", columnDefinition = "bigint(10)", nullable = false)
    LessonPage page

}
