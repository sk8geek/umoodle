/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.workshop

import uk.co.channele.umoodle.domain.lib.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="workshop_assessments")
class WorkshopAssessment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "weight", columnDefinition = "bigint(10)", nullable = false)
    Long weight = 1

    @Column(name = "timecreated", columnDefinition = "bigint(10)")
    Long timecreated = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)")
    Long timemodified = 0

    @Column(name = "grade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal grade

    @Column(name = "gradinggrade", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal gradinggrade

    @Column(name = "gradinggradeover", columnDefinition = "decimal(10,5)", nullable = false)
    BigDecimal gradinggradeover

    @Column(name = "feedbackauthor", columnDefinition = "longtext")
    String feedbackauthor

    @Column(name = "feedbackauthorformat", columnDefinition = "smallint(3)")
    Integer feedbackauthorformat = 0

    @Column(name = "feedbackauthorattachment", columnDefinition = "smallint(3)")
    Integer feedbackauthorattachment = 0

    @Column(name = "feedbackreviewer", columnDefinition = "longtext")
    String feedbackreviewer

    @Column(name = "feedbackreviewerformat", columnDefinition = "smallint(3)")
    Integer feedbackreviewerformat = 0


    @OneToOne()
    @JoinColumn(name = "submissionid", columnDefinition = "bigint(10)", nullable = false)
    WorkshopSubmission submission

    @OneToOne()
    @JoinColumn(name = "reviewerid", columnDefinition = "bigint(10)", nullable = false)
    User reviewer

    @OneToOne()
    @JoinColumn(name = "gradinggradeoverby", columnDefinition = "bigint(10)")
    User gradingGradeOverBy

}
