/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.feedback

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="feedback_item")
class FeedbackItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "label", length = 255, nullable = false)
    String label

    @Column(name = "presentation", columnDefinition = "longtext", nullable = false)
    String presentation

    @Column(name = "typ", length = 255, nullable = false)
    String typ

    @Column(name = "hasvalue", columnDefinition = "tinyint(1)", nullable = false)
    Boolean hasvalue = false

    @Column(name = "position", columnDefinition = "tinyint(3)", nullable = false)
    Integer position = 0

    @Column(name = "required", columnDefinition = "tinyint(1)", nullable = false)
    Boolean required = false

    @Column(name = "dependitem", columnDefinition = "bigint(10)", nullable = false)
    Long dependitem = 0

    @Column(name = "dependvalue", length = 255, nullable = false)
    String dependvalue

    @Column(name = "options", length = 255, nullable = false)
    String options


    @ManyToOne()
    @JoinColumn(name = "feedback", columnDefinition = "bigint(10)", nullable = false)
    Feedback feedback

    @OneToOne
    @JoinColumn(name = "template", columnDefinition = "bigint(10)", nullable = false)
    FeedbackTemplate template

}
