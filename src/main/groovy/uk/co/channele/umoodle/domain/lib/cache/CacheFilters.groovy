/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.cache

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "cache_filters")
class CacheFilters {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 32, nullable = false)
    String filter = ""

    @Column(nullable = false)
    Long version = 0

    @Column(length = 32, nullable = false)
    String md5key = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String rawtext = ""

    @Column(nullable = false)
    Long timemodified = 0


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>

// <INDEX NAME="filter_md5key" UNIQUE="false" FIELDS="filter, md5key"/>
}
