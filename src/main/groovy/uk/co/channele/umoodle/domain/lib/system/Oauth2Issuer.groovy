/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "oauth2_issuer")
class Oauth2Issuer {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long timecreated

    @Column(name = "timemodified", nullable = false)
    Long timeModified

    @Column(nullable = false)
    Long usermodified

    @Column(length = 255, nullable = false)
    String name = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String image = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String baseurl = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String clientid = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String clientsecret = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String loginscopes = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String loginscopesoffline = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String loginparams = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String loginparamsoffline = ""

    @Column(columnDefinition = "longtext", nullable = false)
    String alloweddomains = ""

    @Column(columnDefinition = "longtext")
    String scopessupported = ""

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer enabled = 1

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer showonloginpage = 1

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer basicauth = 0

    @Column(nullable = false)
    Long sortorder

    @Column(columnDefinition = "tinyint", nullable = false)
    Integer requireconfirmation = 1


// <KEY NAME="primary" TYPE="primary" FIELDS="id"/>
}
