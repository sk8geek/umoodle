/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "course_sections", uniqueConstraints = @UniqueConstraint(columnNames = ["course", "section"]))
class CourseSection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(nullable = false)
    Long section = 0

    @Column(length = 255)
    String name = ""

    @Column(columnDefinition = "longtext")
    String summary = ""

    @Column(columnDefinition = "tinyint(2)", nullable = false)
    Integer summaryformat = 0

    @Column(columnDefinition = "longtext")
    String sequence = ""

    @Column(columnDefinition = "bit(1)", nullable = false)
    Integer visible = 1

    @Column(columnDefinition = "longtext")
    String availability = ""

    @Column(nullable = false)
    Long timemodified = 0

    @ManyToOne
    @JoinColumn(name = "course", nullable = false)
    Course course

}
