/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.question.type.numerical

import uk.co.channele.umoodle.domain.lib.question.Question

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name ="question_numerical_units")
class QuestionNumericalUnits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

	@Column(name = "multiplier", columnDefinition = "decimal(38,19)")
	BigDecimal multiplier = 1.00000000000000000000

    @Column(name = "unit", length = 50, nullable = false)
    String unit


    @ManyToOne()
    @JoinColumn(name = "question", columnDefinition = "bigint(10)", nullable = false)
    Question question


//        <INDEX NAME="question-unit" UNIQUE="true" FIELDS="question, unit" COMMENT="Unique index to ensure that only one unit with a particular name is created for each question."/>

}
