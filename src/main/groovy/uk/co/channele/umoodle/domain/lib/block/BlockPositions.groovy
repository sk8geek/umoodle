/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.lib.block

import uk.co.channele.umoodle.domain.lib.context.Context
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "block_positions", uniqueConstraints = @UniqueConstraint(columnNames = ["blockinstanceid", "contextid", "pagetype", "subpage"]))
class BlockPositions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(length = 64, nullable = false)
    String pagetype = ""

    @Column(length = 16, nullable = false)
    String subpage = ""

    @Column(columnDefinition = "smallint", nullable = false)
    Integer visible

    @Column(length = 16, nullable = false)
    String region = ""

    @Column(nullable = false)
    Long weight


    @ManyToOne
    @JoinColumn(name = "blockinstanceid", nullable = false)
    BlockInstances blockInstance

    @ManyToOne
    @JoinColumn(name = "contextid", nullable = false)
    Context context

}
