/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.domain.mod.feedback

import uk.co.channele.umoodle.domain.lib.course.Course

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name ="feedback")
class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column(name = "name", length = 255, nullable = false)
    String name

    @Column(name = "intro", columnDefinition = "longtext", nullable = false)
    String intro

    @Column(name = "introformat", columnDefinition = "smallint(4)", nullable = false)
    Integer introFormat = 0

    @Column(name = "anonymous", columnDefinition = "tinyint(1)", nullable = false)
    Boolean anonymous = true

    @Column(name = "email_notification", columnDefinition = "bit(1)", nullable = false)
    Boolean emailNotification = true

    @Column(name = "multiple_submit", columnDefinition = "bit(1)", nullable = false)
    Boolean multipleSubmit = true

    @Column(name = "autonumbering", columnDefinition = "bit(1)", nullable = false)
    Boolean autoNumbering = true

    @Column(name = "site_after_submit", length = 255, nullable = false)
    String siteAfterSubmit

    @Column(name = "page_after_submit", columnDefinition = "longtext", nullable = false)
    String pageAfterSubmit

    @Column(name = "page_after_submitformat", columnDefinition = "tinyint(2)", nullable = false)
    Integer pageAfterSubmitFormat = 0

    @Column(name = "publish_stats", columnDefinition = "bit(1)", nullable = false)
    Boolean publishStats = false

    @Column(name = "timeopen", columnDefinition = "bigint(10)", nullable = false)
    Long timeOpen = 0

    @Column(name = "timeclose", columnDefinition = "bigint(10)", nullable = false)
    Long timeClose = 0

    @Column(name = "timemodified", columnDefinition = "bigint(10)", nullable = false)
    Long timeModified = 0

    @Column(name = "completionsubmit", columnDefinition = "bit(1)", nullable = false)
    Boolean completionSubmit = false


    @OneToMany(mappedBy = "feedback")
    Set<FeedbackCompleted> completedSet

    @OneToMany(mappedBy = "feedback")
    Set<FeedbackItem> itemSet

    @ManyToOne()
    @JoinColumn(name = "course", columnDefinition = "bigint(10)", nullable = false)
    Course course

}
