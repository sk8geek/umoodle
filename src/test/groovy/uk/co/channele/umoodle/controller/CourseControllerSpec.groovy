/*
 * Copyright (C) 2020  Steven J Lilley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package uk.co.channele.umoodle.controller

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification
import uk.co.channele.umoodle.domain.lib.course.Course
import uk.co.channele.umoodle.domain.lib.course.CourseCategory
import uk.co.channele.umoodle.repository.CourseCategoryRepo
import uk.co.channele.umoodle.repository.CourseRepo

import javax.inject.Inject

@MicronautTest
class CourseControllerSpec extends Specification {

    @Inject
    CourseRepo courseRepo

    @Inject
    CourseCategoryRepo courseCategoryRepo

    @Inject
    @Client("/")
    HttpClient client

    void setup() {

        def category = new CourseCategory(
                name: "Category",
                path: "/1"
        )
        courseCategoryRepo.save(category)

        def course = new Course(
                category: category,
                fullName: "Acme Road Runner Capture or Kill Course",
                shortName: "RoadKill"
        )
        courseRepo.save(course)

    }

    void "retrieve a course"() {

        when: "get a course"
        HttpRequest<?> request = HttpRequest.GET("/courses/1")
        HttpResponse<Course> response = client.toBlocking().exchange(request, Course.class)

        then: "we have a course"
        response.body().shortName == "RoadKill"

    }

}
